<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>BitBrighter Client Manager</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Poppins:300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i&display=swap" rel="stylesheet">

        <!-- Styles -->
        <style>
            
            html, body {
                background-color: #fff;
                color: #ffffff;
                font-family: 'Poppins', sans-serif;
                font-weight: 200;
                height: 100vh;
                margin: 0;
            }

            .full-height {
                height: 100vh;
                background: rgb(63,94,251);
                background: -moz-linear-gradient(349deg, rgba(63,94,251,1) 0%, rgba(70,223,252,1) 100%);
                background: -webkit-linear-gradient(349deg, rgba(63,94,251,1) 0%, rgba(70,223,252,1) 100%);
                background: linear-gradient(349deg, rgba(63,94,251,1) 0%, rgba(70,223,252,1) 100%);
                filter: progid:DXImageTransform.Microsoft.gradient(startColorstr="#3f5efb",endColorstr="#46dffc",GradientType=1);
            }

            .flex-center {
                align-items: center;
                display: flex;
                justify-content: center;
            }

            .position-ref {
                position: relative;
            }

            .top-right {
                position: absolute;
                right: 10px;
                top: 18px;
            }

            .content {
                text-align: center;
            }

            .title {
                font-size: 84px;
            }
            
            .content .title{ text-align: center; text-shadow: 1px 1px 1px rgba(0,0,0,0.15); }
            .content .title img{ 
                height:128px; 
                display:inline-block;
                vertical-align: top;
                border-radius:50%;
                overflow: hidden;
                box-shadow:1px 1px 24px #fff;
            }
            .content .title span{ 
                display:inline-block;
                vertical-align: top;
            }
            
            .links > a {
                color: #ffffff;
                padding: 0 25px;
                font-size: 13px;
                font-weight: 600;
                letter-spacing: .1rem;
                text-decoration: none;
                text-transform: uppercase;
                text-shadow: 1px 1px 1px rgba(0,0,0,0.15);
            }

            .m-b-md {
                margin-bottom: 30px;
            }
            
            .footer{
                position:absolute;
                bottom:60px;
                width:100%;
                text-align: center;
                text-shadow: 1px 1px 1px rgba(0,0,0,0.15);
            }
        </style>
    </head>
    <body>
        <div class="flex-center position-ref full-height">
            @if (Route::has('login'))
                <div class="top-right links">
                    @auth
                        @if(Auth::user()->isAdmin())
                        <a href="{{ url('/admin') }}">Dashboard</a>
                        @else
                        <a href="{{ url('/dashboard') }}">Dashboard</a>
                        @endif
                    @else
                        <a href="{{ route('login') }}">Login</a>

                        @if (Route::has('register'))
                            <a href="{{ route('register') }}">Register</a>
                        @endif
                    @endauth
                </div>
            @endif

            <div class="content">
                <div class="title m-b-md">
                    @php $logo = App\Settings::get_setting('company_logo'); @endphp
                    @if($logo)
                    <img src='{{url($logo)}}' height='128'>
                    @endif
                    
                    <span>Client Manager</span>
                </div>

                <!--<div class="links">
                    <a href="https://laravel.com/docs">Docs</a>
                    <a href="https://laracasts.com">Laracasts</a>
                    <a href="https://laravel-news.com">News</a>
                    <a href="https://blog.laravel.com">Blog</a>
                    <a href="https://nova.laravel.com">Nova</a>
                    <a href="https://forge.laravel.com">Forge</a>
                    <a href="https://github.com/laravel/laravel">GitHub</a>
                </div>-->
            </div>
            
            
        </div>
        
        <footer class="grid-item footer">&copy; Copyright BitBrighter PTY LTD 2019</footer>
        
    </body>
</html>
