@extends('layouts.app')

@section('content')

    <div class="card">
        
        <div class="card-header hasicon">
            <div class='card-header-icon svg-icon'>@include('icons.dashboard')</div>
            <div class='card-header-text'>Admin Dashboard</div>
        </div>

        <div class="card-body">
            @if (session('status'))
                <div class="alert alert-success alert-dismissible fade show" role="alert">
                    {!! session('status') !!}
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            @endif

            Welcome Administrator!
        </div>

    </div>

@endsection
