<!-- Modal -->
<div class="modal fade" id="addProjectModal" tabindex="-1" role="dialog" aria-labelledby="addProjectModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <form action="{{ route('admin.projects.add') }}" method='post'>

                @csrf

                <!--<input id="project_user_id" type="hidden" class="form-control" name="project_user_id" value="{{ $client->id }}">-->

                <div class="modal-header">
                    <h5 class="modal-title" id="addProjectModalLabel">Create New Project</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    
                    @if(isset($clients))
                    <div class="form-group row">
                        <label for="project_user_id" class="col-md-2 col-form-label text-md-right">{{ __('Client:') }}</label>
                        <div class="col-md-10">
                            <select name='project_user_id' class="form-control">
                                <option>Select Client</option>
                                @foreach($clients as $client)
                                <option value='{{ $client->id }}'>{{ $client->name }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    @endif
                    
                    <div class="form-group row">
                        <label for="project_name" class="col-md-2 col-form-label text-md-right">{{ __('Name:') }}</label>
                        <div class="col-md-10">
                            <input id="project_name" type="text" class="form-control" name="project_name" value="" required>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="project_brief" class="col-md-2 col-form-label text-md-right">{{ __('Brief:') }}</label>
                        <div class="col-md-10">
                            <input id="project_brief" type="text" class="form-control" name="project_brief" value="" required>
                        </div>
                    </div>

                    <!--<div class="form-group row">
                        <label for="project_estimated_time" class="col-md-2 col-form-label text-md-right">{{ __('Estimated Time:') }}</label>
                        <div class="col-md-10">
                            <input id="project_estimated_time" type="text" class="form-control" name="project_estimated_time" value="">
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="project_lapsed_time" class="col-md-2 col-form-label text-md-right">{{ __('Lapsed Time:') }}</label>
                        <div class="col-md-10">
                            <input id="project_lapsed_time" type="text" class="form-control" name="project_lapsed_time" value="">
                        </div>
                    </div>-->

                    <div class="form-group row">
                        <label for="project_due_date" class="col-md-2 col-form-label text-md-right">{{ __('Due Date:') }}</label>
                        <div class="col-md-10">
                            <input id="project_due_date" type="text" class="form-control" name="project_due_date" value="" data-provide="datepicker" data-date-format="yyyy-mm-dd" required>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="project_rate" class="col-md-2 col-form-label text-md-right">{{ __('Hourly Rate:') }}</label>
                        <div class="col-md-10">
                            <input id="project_rate" type="text" class="form-control" name="project_rate" value="{{App\Settings::get_setting('company_rate')}}" required>
                        </div>
                    </div>
                
                
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                    <button type="submit" class="btn btn-primary">{{ __('Create Project') }}</button>
                </div>
            </form>
        </div>
    </div>
</div>