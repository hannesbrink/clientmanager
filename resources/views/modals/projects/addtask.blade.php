<!-- Modal -->
<div class="modal fade" id="addProjectTaskModal" tabindex="-1" role="dialog" aria-labelledby="addProjectTaskModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
        <div class="modal-content">
            <form action="{{ route('admin.projects.task.add', $project->id) }}" method='post'>

                @csrf

                <input id="project_user_id" type="hidden" class="form-control" name="project_user_id" value="{{ $client->id }}">

                <div class="modal-header">
                    <h5 class="modal-title" id="addProjectTaskModalLabel">Add Task</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">

                    <div class="form-group row">
                        <label for="task_estimated_time" class="col-md-3 col-form-label text-md-right">{{ __('Estimated Time:') }}</label>
                        <div class="col-md-9">
                            <input id="task_estimated_time" type="text" class="form-control" name="task_estimated_time" value="" required>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="task_brief" class="col-md-3 col-form-label text-md-right">{{ __('Description:') }}</label>
                        <div class="col-md-9">
                            <input id="task_brief" type="text" class="form-control" name="task_brief" value="" required>
                        </div>
                    </div>


                    <!--<div class="form-group row">
                        <label for="project_estimated_time" class="col-md-3 col-form-label text-md-right">{{ __('Estimated Time:') }}</label>
                        <div class="col-md-9">
                            <input id="project_estimated_time" type="text" class="form-control" name="project_estimated_time" value="">
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="project_lapsed_time" class="col-md-3 col-form-label text-md-right">{{ __('Lapsed Time:') }}</label>
                        <div class="col-md-9">
                            <input id="project_lapsed_time" type="text" class="form-control" name="project_lapsed_time" value="">
                        </div>
                    </div>-->

                    <!--<div class="form-group row">
                        <label for="project_due_date" class="col-md-3 col-form-label text-md-right">{{ __('Elapsed Time:') }}</label>
                        <div class="col-md-9">
                            <input id="project_due_date" type="text" class="form-control" name="project_due_date" value="" data-provide="datepicker" data-date-format="dd/mm/yyyy" required>
                        </div>
                    </div>-->

                    <!--<div class="form-group row">
                        <label for="project_rate" class="col-md-3 col-form-label text-md-right">{{ __('Hourly Rate:') }}</label>
                        <div class="col-md-9">
                            <input id="project_rate" type="text" class="form-control" name="project_rate" value="App\Settings::get_setting('company_rate')" required>
                        </div>
                    </div>-->
                
                
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                    <button type="submit" class="btn btn-success">{{ __('Add Task') }}</button>
                </div>
            </form>
        </div>
    </div>
</div>