<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
	<title>{{ $title }}</title>
</head>
<body>

	<style type="text/css">

	    @font-face {
	        font-family: 'OpenSans';
	        src: url("{{ storage_path('fonts/OpenSans-Regular.ttf') }}") format('truetype');
	        font-weight: normal;
	        font-style: normal;
	    }
	    @font-face {
	        font-family: 'OpenSansSemiBold';
	        src: url("{{ storage_path('fonts/OpenSans-SemiBold.ttf') }}") format('truetype');
	        font-weight: 500;
	        font-style: normal;
	    }

	    @font-face {
	        font-family: 'Lato';
	        src: url("{{ storage_path('fonts/Lato-Regular.ttf') }}") format('truetype');
	        font-weight: normal;
	        font-style: normal;
	    }
	    @font-face {
	        font-family: 'LatoBold';
	        src: url("{{ storage_path('fonts/Lato-Bold.ttf') }}") format('truetype');
	        font-weight: 600;
	        font-style: normal;
	    }

	    html, body{ margin:0; padding:0; }
	    body{ 
	    	font-family: 'Lato', sans-serif;
	    	font-size:12px;
	    	line-height:12px;
	    	color:#686868;
	    	position:relative;
	    }

	    strong{
	    	font-family: 'Lato', sans-serif; font-weight: 500;
	    }

	    h1, h2, h3, h4{ 
	    	margin:0; 
	    	padding:0; 
	    	font-family: 'LatoBold', sans-serif; 
	    	font-weight: 500; 
	    	color:#686868; 
	    }

	    h1{ padding:20px 0 10px; }
	    h3{ 
	    	padding-top:15px; 
	    	padding-bottom:5px; 
	    	margin-bottom:5px;
	    	font-size:14px;
	    	border-bottom:3px solid #F3F3F3;
	    }

		.page-break {
		    page-break-after: always;
		}

		#header{
			background-color:#11AFF1;
			padding:50px 50px 0;
		}
		#header .header-card{
			background-color: #F9FAFC;
			border-top-left-radius: 5px;
			border-top-right-radius: 5px;
			padding:40px 40px 0;
		}

	</style>

	<div id='header'>
		<div class='header-card'>
		  	<table width="100%" style="width:100%; margin-bottom:20px;" border="0">
				<tr>
					<td width='47.5%' valign='top'>
						@php $imageurl = App\Settings::get_setting('company_logo'); @endphp
						<img src="{{ $imageurl }}" width="100">
					</td>
					<td width='5%' valign='top'>
					<td width='47.5%' valign='top' align="right">
						<h1>QUOTE: 015</h1>
						<h2>{{ date('d/m/Y', $timestamp) }}</h2>
					</td>
				</tr>
			</table>
		</div>
	</div>

	<table width="100%" style="width:100%" border="0">
		<tr>
			<td width='47.5%' valign='top'>
				<div class='divblock'>
					<h3>TO</h3>
					<div><strong>Craig Killeen</strong><br>Nevagivuppe CC t/a MedInformer<br>24 King Street, Hout Bay, 7806<br>info@medinformer.co.za </div>
				</div>
			</td>
			<td width='5%' valign='top'>
			<td width='47.5%' valign='top'>
				<div class='divblock'>
					<h3>FROM</h3>
					<div><strong>Hannes Brink</strong><br>3 Liberty Court, 31 St. James Str.<br>Vrederhoek, Cape Town<BR>8001<BR>0713726779<br>han@bitbrighter.com</div><br>
				</div>
			</td>
		</tr>
    </table>



	// TABLE GRID HERE... 
	


	<table width="100%" style="width:100%" border="0">
		<tr>
			<td valign='top'>
				<h3>PLEASE NOTE</h3>
				<div>
					50% deposit required before development can start.<br>
					Final deposit on completion.<br>
				</div>
			</td>
		</tr>
	</table>

	<table width="100%" style="width:100%" border="0">
		<tr>
			<td width='47.5%' valign='top'>
				<div class='divblock'>
					<h3>PROJECT JOB</h3>
					<div><strong>Hannes Brink</strong><br>3 Liberty Court, 31 St. James Str.<br>Vrederhoek, Cape Town<BR>8001<BR>0713726779<br>han@bitbrighter.com</div><br>
				</div>
			</td>
			<td width='5%' valign='top'>
			<td width='47%' valign='top'>
				<div class='divblock'>
					<h3>BANKING DETAILS</h3>
					<div>
						<strong>Account Name:</strong> J.P.BRINK<br>
						<strong>Bank:</strong> FNB (MOWBRAY)<br>
						<strong>Account Type:</strong> CHEQUE ACCOUNT<br>
						<strong>Account No:</strong> 627 8328 1662<br>
						<strong>Branch Code:</strong> 250655
					</div>					
				</div>
			</td>
		</tr>
    </table>


	
	<div class="page-break"></div>

	<div class='divblock'>
		<h3>PAYMENT TERMS</h3>
		<div>
			50% deposit required before development can start.<br>
			Final deposit on completion.<br>
		</div>
	</div>
	<div class='divblock'>
		<h3>BANK ACCOUNT DETAILS</h3>
		<div>
			<strong>Account Name:</strong> J.P.BRINK<br>
			<strong>Bank:</strong> FNB (MOWBRAY)<br>
			<strong>Account Type:</strong> CHEQUE ACCOUNT<br>
			<strong>Account No:</strong> 627 8328 1662<br>
			<strong>Branch Code:</strong> 250655
		</div>					
	</div>
	<div>
		<p>{{ $content }}</p>
	</div>
</body>
</html>