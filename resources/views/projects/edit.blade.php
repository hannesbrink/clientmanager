@extends('layouts.app')

@section('content')
    
    <div class="card">
        
        <div class="card-header hasicon">
            <div class='card-header-icon svg-icon'>@include('icons.projects')</div>
            <div class='card-header-text'>Projects</div>
            <div class='card-header-button'>
                <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#addProjectModal">Add New Project</button>
            </div>
        </div>

        <div class="card-body">
            @if (session('status'))
                <div class="alert alert-success" role="alert">
                    {{ session('status') }}
                </div>
            @endif

            @if(isset($project))
            <form action="{{ route('projects.update', $project->id) }}" method='post'>

                @csrf

                <div class="form-group row">
                    <label for="project_name" class="col-md-2 col-form-label text-md-right">{{ __('Name:') }}</label>
                    <div class="col-md-10">
                        <input id="project_name" type="text" class="form-control" name="project_name" value="{{ $project->name }}">
                    </div>
                </div>

                <div class="form-group row">
                    <label for="project_brief" class="col-md-2 col-form-label text-md-right">{{ __('Brief:') }}</label>
                    <div class="col-md-10">
                        <input id="project_brief" type="text" class="form-control" name="project_brief" value="{{ $project->brief }}">
                    </div>
                </div>

                    <!--<div class="form-group row">
                        <label for="project_estimated_time" class="col-md-2 col-form-label text-md-right">{{ __('Estimated Time:') }}</label>
                        <div class="col-md-10">
                            <input id="project_estimated_time" type="text" class="form-control" name="project_estimated_time" value="">
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="project_lapsed_time" class="col-md-2 col-form-label text-md-right">{{ __('Lapsed Time:') }}</label>
                        <div class="col-md-10">
                            <input id="project_lapsed_time" type="text" class="form-control" name="project_lapsed_time" value="">
                        </div>
                    </div>-->

                <div class="form-group row">
                    <label for="project_due_date" class="col-md-2 col-form-label text-md-right">{{ __('Due Date:') }}</label>
                    <div class="col-md-10">
                        <input id="project_due_date" type="text" class="form-control" name="project_due_date" value="{{ $project->due_date }}" data-provide="datepicker" data-date-format="yyyy-mm-dd">
                    </div>
                </div>
                
                <div class="form-group row mb-0">
                    <div class="col-md-10 offset-md-2">
                        <button type="submit" class="btn btn-primary">
                            {{ __('Update') }}
                        </button>
                    </div>
                </div>

            </form>

            @endif
        </div>

    </div>

    @include('modals.projects.add')

@endsection
