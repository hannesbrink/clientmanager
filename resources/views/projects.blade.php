@extends('layouts.app')

@section('content')
    
    <div class="card">
        
        <div class="card-header hasicon">
            <div class='card-header-icon svg-icon'>@include('icons.projects')</div>
            <div class='card-header-text'>Projects</div>
            <div class='card-header-button'>
                <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#addProjectModal">Create New Project</button>
            </div>
        </div>

        <div class="card-body">
            @if (session('status'))
                <div class="alert alert-success alert-dismissible fade show" role="alert">
                    {!! session('status') !!}
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            @endif
            <!--<pre>{{ print_r($projects,true) }}</pre>-->
            @if(isset($projects) && count($projects))
                <div class='project-cards'>
                    @foreach($projects as $project)
                    <a href="{{ route('projects.edit', $project->id) }}" class='project-card'>
                        <div class='project-card-name'><h2>{{ $project->name }}</h2></div>
                        <div class='project-card-duedate'><strong>Due:</strong> {{ $project->due_date }}</div>
                        <div class='project-card-brief'>{{ $project->brief }}</div>
                        <!--<div class='project-card-status'>{{ $project->status }}</div>-->
                    </a>
                    @endforeach
                </div>
            @else
                <div>No projects found on your profile. <a href="#" data-toggle="modal" data-target="#addProjectModal">Create a Project</a></div>
            @endif
        </div>

    </div>

    @include('modals.projects.add')

@endsection
