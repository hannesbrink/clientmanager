


@extends('layouts.app')

@section('content')

    <div class="card">
        
        <div class="card-header hasicon">
            <div class='card-header-icon svg-icon'>@include('icons.clients')</div>
            <div class='card-header-text'>Administrator Clients</div>
            <div class='card-header-button'>
                <!--<button type="button" class="btn btn-success" data-toggle="modal" data-target="#addProjectTaskModal">Add Task</button>
                <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#addProjectModal">Create New Project</button>-->
                
                <a href="{{ URL::to('/register') }}" target="_blank" class="btn btn-primary">Register New Client</a>
            </div>
        </div>

        <div class="card-body">
            @if (session('status'))
                <div class="alert alert-success alert-dismissible fade show" role="alert">
                    {!! session('status') !!}
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            @endif


            <table id='clients_table' class='table' width="100%">
                <tr>
                    <th>Name</th>
                    <th>Projects</th>
                    <th>Status</th>
                    <th>Color</th>
                    <th colspan='2'>Actions</th>
                </tr>
                @foreach($clients as $client)
                <tr>
                    <td width='320' class='column-name'>
                        <div class='clientname'><strong>{{ $client->name }}</strong></div>
                        <div class='clientemail'><a href='mailto:{{ $client->email }}'><small>{{ $client->email }}</small></a></div>
                        <div class='clientcompany'><small>{{ $client->company }}</small></div>
                    </td>
                    <td class='column-projects'>
                    @php
                        
                        $projects = App\User::getUserProjects($client->id);
                        if(isset($projects)):
                        foreach($projects as $project){
                            @endphp 
                            <div><a class='inner-link {{ $project->status }} animate' href="{{ route('admin.projects.edit', $project->id) }}">{{ $project->name }}</a></div>
                            @php
                        }
                        endif;
                    @endphp
                    </td>
                    <td width='71' class='column-status'>{{ $client->status }}</td>
                    <td width='68' class='column-color' align="center"><div class='colorswab' style='background-color: {{ $client->color }};'></div></td>
                    <td width='41'><a href='{{ route("admin.client.edit", $client->id) }}' class='btn btn-primary btn-sm'>Edit</a></td>
                    <td width='58'><a href='{{ route("admin.client.delete", $client->id) }}' class='btn btn-danger btn-sm'>Delete</a></td>
                </tr>
                @endforeach
            </table>
        </div>

    </div>

@endsection
