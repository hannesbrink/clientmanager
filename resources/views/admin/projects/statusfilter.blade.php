
            
                <div class='filter status'>
                    <div class='filter-dropdown-label'>Status:</div>
                    <div class='filter-dropdown'>
                        <select id='project-status-filter' class='form-control filter-select'>
                            <option value='all'>All</option>
                            <option value='quote'>Quote</option>
                            <option value='processing'>Processing</option>
                            <option value='invoice'>Invoice</option>
                            <option value='unpaid'>Unpaid</option>
                            <option value='complete'>Complete</option>
                        </select>
                    </div>
                </div>


