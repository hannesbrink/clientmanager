@if(isset($clients))
<div class='projects-filter'>
    <div class='filter-dropdown-label'>Filter by Clients:</div>
    <div class='filter-dropdown'>
        <select class="form-control">
            <option value=''>Select Client</option>   
            @foreach($clients as $c)
            <option value='{{$c->id}}'>{{ $c->name }}</option>    
            @endforeach 
        </select>
    </div>
</div>
@endif

