@extends('layouts.app')

@section('content')
    
    <div class="card">
        
        <div class="card-header hasicon">
            <div class='card-header-icon svg-icon'>@include('icons.projects')</div>
            <div class='card-header-text'>
                <a href="{{ route('admin.projects') }}">Admin Projects</a> / 
                <a href="{{ route('admin.client.projects', $client->id) }}">{{ $client->name }}</a> / 
                {{ $project->name }}
            </div>
            <div class='card-header-button'>
                <!--<button type="button" class="btn btn-success" data-toggle="modal" data-target="#addProjectTaskModal">Add Task</button>
                <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#addProjectModal">Create New Project</button>-->
                
                <form action="{{ route('admin.projects.delete', $project->id) }}" method='post'>
                    @csrf  
                    
                    <button type="submit" class="btn btn-danger">Delete Project</button>
                    
                    @if($project->status === 'quote')
                        @if(isset($quotes) && count($quotes) == 0)
                            <a href="{{ route('admin.projects.quote', [$project->id, 'view']) }}" target="_blank" class="btn btn-primary generatepdf">Generate Quote</a>
                        @endif
                    @endif
                    
                    @if($project->status === 'invoice' || $project->status === 'complete' || $project->status === 'unpaid')
                        @if(isset($invoices) && count($invoices) == 0)
                            <a href="{{ route('admin.projects.invoice', [$project->id, 'view']) }}" target="_blank" class="btn btn-warning generatepdf {{ $project->status }}">Generate Invoice</a>
                        @endif
                    @endif
                </form>    
            </div>
        </div>

        <div class="card-body">
            @if (session('status'))
                <div class="alert alert-success alert-dismissible fade show" role="alert">
                    {!! session('status') !!}
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            @endif

            @if(isset($project))
            <div class='project-edit-form'>
                <form action="{{ route('admin.projects.update', $project->id) }}" method='post'>

                    @csrf

                    <div class="form-group row">
                        <label for="project_name" class="col-md-2 col-form-label text-md-right">{{ __('Name:') }}</label>
                        <div class="col-md-10">
                            <input id="project_name" type="text" class="form-control" name="project_name" value="{{ $project->name }}">
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="project_brief" class="col-md-2 col-form-label text-md-right">{{ __('Brief:') }}</label>
                        <div class="col-md-10">
                            <input id="project_brief" type="text" class="form-control" name="project_brief" value="{{ $project->brief }}">
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="project_rate" class="col-md-2 col-form-label text-md-right">{{ __('Hourly Rate:') }}</label>
                        <div class="col-md-10">
                            <input id="project_rate" type="text" class="form-control" name="project_rate" value="{{ $project->rate }}">
                        </div>
                    </div>

                        <!--<div class="form-group row">
                            <label for="project_estimated_time" class="col-md-2 col-form-label text-md-right">{{ __('Estimated Time:') }}</label>
                            <div class="col-md-10">
                                <input id="project_estimated_time" type="text" class="form-control" name="project_estimated_time" value="">
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="project_lapsed_time" class="col-md-2 col-form-label text-md-right">{{ __('Lapsed Time:') }}</label>
                            <div class="col-md-10">
                                <input id="project_lapsed_time" type="text" class="form-control" name="project_lapsed_time" value="">
                            </div>
                        </div>-->

                    <div class="form-group row">
                        <label for="project_due_date" class="col-md-2 col-form-label text-md-right">{{ __('Due Date:') }}</label>
                        <div class="col-md-10">
                            <input id="project_due_date" type="text" class="form-control" name="project_due_date" value="{{ date('Y-m-d', strtotime($project->due_date)) }}" data-provide="datepicker" data-date-format="yyyy-mm-dd">
                        </div>
                    </div>
                    
                    <div class="form-group row">
                        <label for="project_status" class="col-md-2 col-form-label text-md-right">{{ __('Status:') }}</label>
                        <div class="col-md-10">
                            <select name='project_status' class="form-control">
                                <option>Select Status</option>
                                <option value='quote' {{ ($project->status == 'quote')? 'selected': '' }}>Quote</option>
                                <option value='processing' {{ ($project->status == 'processing')? 'selected': '' }}>Processing</option>
                                <option value='invoice' {{ ($project->status == 'invoice')? 'selected': '' }}>Invoice</option>
                                <option value='unpaid' {{ ($project->status == 'unpaid')? 'selected': '' }}>Unpaid</option>
                                <option value='complete' {{ ($project->status == 'complete')? 'selected': '' }}>Complete</option>
                            </select>
                        </div>
                    </div>
                    @if($project->status == 'unpaid')
                    <div class="form-group row unpaid-amount animate active">
                    @else
                    <div class="form-group row unpaid-amount animate">
                    @endif
                        <label for="project_paid_amount" class="col-md-2 col-form-label text-md-right">{{ __('Total Paid:') }}</label>
                        <div class="col-md-10">
                            <input id="project_paid_amount" type="text" class="form-control" name="project_paid_amount" placeholder="Enter the total that has been paid so far." value="{{ number_format($project->paid_amount, 2, '.', '') }}">
                        </div>
                    </div>
                    
                    <div class="form-group row mb-0">
                        <div class="col-md-10 offset-md-2">
                            
                            <button type="submit" class="btn btn-primary">
                                {{ __('Update') }}
                            </button>
                            
                        </div>
                    </div>

                </form>
            </div>
            
           
            <input type='hidden' id='deletetask_url' value='{{ route("admin.task.delete") }}'>
            <input type='hidden' id='addtask_url' value='{{ route("admin.projects.task.addjson") }}'>
            <input type='hidden' id='sorttask_url' value='{{ route("admin.projects.task.sortjson") }}'>
            <input type='hidden' id='projectid' value='{{ $project->id }}'>
            <div id='empty-task-entry'>
                <li>
                <div class='project-task incomplete'>
                   
                    <div class='project-task-dragdrop'>
                        <svg class="Icon DragIcon DragHandle-icon" focusable="false" viewBox="0 0 32 32"><path d="M14,5.5c0,1.7-1.3,3-3,3s-3-1.3-3-3s1.3-3,3-3S14,3.8,14,5.5z M21,8.5c1.7,0,3-1.3,3-3s-1.3-3-3-3s-3,1.3-3,3S19.3,8.5,21,8.5z M11,12.5c-1.7,0-3,1.3-3,3s1.3,3,3,3s3-1.3,3-3S12.7,12.5,11,12.5z M21,12.5c-1.7,0-3,1.3-3,3s1.3,3,3,3s3-1.3,3-3S22.7,12.5,21,12.5z M11,22.5c-1.7,0-3,1.3-3,3s1.3,3,3,3s3-1.3,3-3S12.7,22.5,11,22.5z M21,22.5c-1.7,0-3,1.3-3,3s1.3,3,3,3s3-1.3,3-3S22.7,22.5,21,22.5z"></path></svg>
                    </div>
                    
                    @if($project->status !== 'quote')<div class='project-task-icon'></div>@endif
                    <div class='project-task-time'>
                        @include('admin.projects.tasktimedropdown')
                    </div>
                    <div class='project-task-desc'>
                        <textarea class="task-textarea animate" placeholder="Enter task description here..." rows="1"></textarea>
                    </div>
                    <!--<div class='project-task-actions'>
                        <a href='#' class='calendar-action'><div class='calendar-action-icon animate'></div></a>
                        <a href='#' class='user-assign-action'><div class='user-assign-action-icon animate'></div></a>
                        <a href="#" class='delete-task-action'><div class='delete-task-action-icon animate'></div></a>
                    </div>-->
                    
                </div>
                </li>
            </div>
            
                
            <div class='project-menu'>   
                <ul class='nav nav-tabs'>
                  <li class="active"><a href="#" data-target="#project-tasks">Tasks</a></li>
                  <li><a href="#" data-target="#project-quotes">Quotes</a></li>
                  <li><a href="#" data-target="#project-invoices">Invoices</a></li>
                </ul>                
            </div>
                
                
            <div id='project-invoices' class='project-menu-page collapse'>
                <h2 class='project-section-title'>PROJECT INVOICES</h2>
                @if(isset($invoices) && count($invoices) > 0)
                <table class='table'>
                    <tr>
                        <th>Id</th>
                        <th>Name</th>
                        <th colspan='2'>Actions</th>
                    </tr>
                    @foreach($invoices as $invoice)
                    <tr>
                        <td width='50'>#{{ (App\Settings::get_setting('company_invoice_start') + $invoice->id) }}</td>
                        <td>{{ $invoice->name }}</td>
                        <td width='42'><a href='{{ route("admin.projects.invoice.open", [$invoice->id, "view"]) }}' class='btn btn-primary btn-sm' target="_blank">Open</a></td>
                        <td width='83'>
                            <form action="{{ route('admin.projects.invoice.delete', $invoice->id) }}" method='post'>
                                @csrf
                                <button type='submit' class='btn btn-danger btn-sm'>Delete</button>
                            </form>
                        <!--    <a href='{{ route("admin.projects.invoice.delete", $invoice->id) }}'>Delete</a>-->
                        </td>
                    </tr>
                    @endforeach
                </table>
                @else
                <div class="alert alert-warning alert-dismissible fade show" role="alert">
                    <em>No invoices found for this project.</em>
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                @endif
                
            </div>
            <div id="project-quotes" class="project-menu-page collapse">
                <h2 class='project-section-title'>PROJECT QUOTES</h2>
                @if(isset($quotes) && count($quotes) > 0)
                <table class='table'>
                    <tr>
                        <th>Id</th>
                        <th>Name</th>
                        <th colspan='2'>Actions</th>
                    </tr>
                    @foreach($quotes as $quote)
                    <tr>
                        <td width='50'>#{{ (App\Settings::get_setting('company_quote_start') + $quote->id) }}</td>
                        <td>{{ $quote->name }}</td>
                        <td width='42'><a href='{{ route("admin.projects.quote.open", [$quote->id, "view"]) }}' class='btn btn-primary btn-sm' target="_blank">Open</a></td>
                        <td width='83'>
                            <form action="{{ route('admin.projects.quote.delete', $quote->id) }}" method='post'>
                                @csrf
                                <button type='submit' class='btn btn-danger btn-sm'>Delete</button>
                            </form>
                        </td>
                    </tr>
                    @endforeach
                </table>
                @else
                <div class="alert alert-warning alert-dismissible fade show" role="alert">
                    <em>No quotes found for this project.</em>
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>                    
                </div>
                @endif
            </div>
            <div id='project-tasks' class='project-menu-page project-tasks'>
                <h2 class='project-section-title'>PROJECT TASKS</h2>
                @if(isset($tasks) && count($tasks) > 0)
                    <ol class='serialization vertical'>
                    @php $cnt = 0; @endphp
                    @foreach($tasks as $task)
                        <li>
                        <div class='project-task {{$task->status}}' data-taskid='{{$task->id}}' data-sortid='{{ $cnt }}'>
                            <div class='project-task-dragdrop'>
                                <svg class="Icon DragIcon DragHandle-icon" focusable="false" viewBox="0 0 32 32"><path d="M14,5.5c0,1.7-1.3,3-3,3s-3-1.3-3-3s1.3-3,3-3S14,3.8,14,5.5z M21,8.5c1.7,0,3-1.3,3-3s-1.3-3-3-3s-3,1.3-3,3S19.3,8.5,21,8.5z M11,12.5c-1.7,0-3,1.3-3,3s1.3,3,3,3s3-1.3,3-3S12.7,12.5,11,12.5z M21,12.5c-1.7,0-3,1.3-3,3s1.3,3,3,3s3-1.3,3-3S22.7,12.5,21,12.5z M11,22.5c-1.7,0-3,1.3-3,3s1.3,3,3,3s3-1.3,3-3S12.7,22.5,11,22.5z M21,22.5c-1.7,0-3,1.3-3,3s1.3,3,3,3s3-1.3,3-3S22.7,22.5,21,22.5z"></path></svg>
                            </div>
                            @if($project->status !== 'quote')
                            <div class='project-task-icon'>
                                <input type='hidden' name='task-status' class='task-status' value='{{ $task->status }}'>
                            </div>
                            @endif
                            <div class='project-task-time'>
                                @php
                                    $timeslots = array(
                                        '0.25', '0.50', '0.75', '1.00',
                                        '1.25', '1.50', '1.75', '2.00',
                                        '2.25', '2.50', '2.75', '3.00',
                                        '3.25', '3.50', '3.75', '4.00',
                                        '4.25', '4.50', '4.75', '5.00',
                                        '5.25', '5.50', '5.75', '6.00',
                                        '6.25', '6.50', '6.75', '7.00',
                                        '7.25', '7.50', '7.75', '8.00',
                                        '8.25', '8.50', '8.75', '9.00',
                                        '9.25', '9.50', '9.75', '10.00',
                                        '10.25', '10.50', '10.75', '11.00',
                                        '11.25', '11.50', '11.75', '12.00',
                                        '12.25', '12.50', '12.75', '13.00',
                                        '13.25', '13.50', '13.75', '14.00',
                                        '14.25', '14.50', '14.75', '15.00',
                                        '15.25', '15.50', '15.75', '16.00',
                                        '16.25', '16.50', '16.75', '17.00',
                                        '17.25', '17.50', '17.75', '18.00',
                                        '18.25', '18.50', '18.75', '19.00',
                                        '19.25', '19.50', '19.75', '20.00',
                                        'Heading',
                                    );
                                    $tasktime = $task->estimate_time;
                                @endphp
                                <select name='task_time' class='task-time'>
                                    @foreach($timeslots as $timeslot)
                                        <option value='{{ $timeslot }}' {{ ($tasktime == $timeslot)? 'selected':'' }}>{{ $timeslot }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class='project-task-desc'>
                                <textarea class="task-textarea animate" placeholder="Enter task description here..." rows="1">{{ $task->brief }}</textarea>
                            </div>
                        </div>
                        </li>
                        @php
                        $cnt++;
                        @endphp
                    @endforeach
                    </ol>
                @else
                <ol class='serialization vertical'>
                    <li>
                    <div class='project-task incomplete'>
                        <div class='project-task-dragdrop'>
                            <svg class="Icon DragIcon DragHandle-icon" focusable="false" viewBox="0 0 32 32"><path d="M14,5.5c0,1.7-1.3,3-3,3s-3-1.3-3-3s1.3-3,3-3S14,3.8,14,5.5z M21,8.5c1.7,0,3-1.3,3-3s-1.3-3-3-3s-3,1.3-3,3S19.3,8.5,21,8.5z M11,12.5c-1.7,0-3,1.3-3,3s1.3,3,3,3s3-1.3,3-3S12.7,12.5,11,12.5z M21,12.5c-1.7,0-3,1.3-3,3s1.3,3,3,3s3-1.3,3-3S22.7,12.5,21,12.5z M11,22.5c-1.7,0-3,1.3-3,3s1.3,3,3,3s3-1.3,3-3S12.7,22.5,11,22.5z M21,22.5c-1.7,0-3,1.3-3,3s1.3,3,3,3s3-1.3,3-3S22.7,22.5,21,22.5z"></path></svg>
                        </div>
                        @if($project->status !== 'quote')<div class='project-task-icon'></div>@endif
                        <div class='project-task-time'>
                            @include('admin.projects.tasktimedropdown')
                        </div>
                        <div class='project-task-desc'>
                            <textarea class="task-textarea animate" placeholder="Enter task description here..." rows="1"></textarea>
                        </div>
                        <!--<div class='project-task-actions'>
                            <a href='#' class='calendar-action'><div class='calendar-action-icon animate'></div></a>
                            <a href='#' class='user-assign-action'><div class='user-assign-action-icon animate'></div></a>
                            <a href="#" class='delete-task-action'><div class='delete-task-action-icon animate'></div></a>
                        </div>-->
                    </div>
                    </li>
                </ol>
                @endif


            </div>
            @endif
        </div>

    </div>

    @include('modals.projects.add')

@endsection
