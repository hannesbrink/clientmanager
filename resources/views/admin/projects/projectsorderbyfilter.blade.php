            
                <div class='filter orderby'>
                    <div class='filter-dropdown-label'>Order By:</div>
                    <div class='filter-dropdown'>
                        <select id='orderby-filter' class='form-control filter-select'>
                            @if( Request::url() == URL::to("/admin/projects") )
                                <option value="{{ route('admin.projects') }}" selected='selected'>Due Date</option>
                            @else
                                <option value="{{ route('admin.projects') }}">Due Date</option>
                            @endif
                            
                            @if( Request::url() == URL::to("/admin/projects/filter/total") )
                                <option value="{{ route('admin.filter.projects.total') }}" selected='selected'>Total</option>
                            @else
                                <option value="{{ route('admin.filter.projects.total') }}">Total</option>
                            @endif
                            
                        </select>
                    </div>
                </div>
            



