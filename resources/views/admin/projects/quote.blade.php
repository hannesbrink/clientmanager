<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
	<title>{{ $data['title'] }}</title>
    
</head>
<body>

	<style type="text/css">


	    @font-face {
	        font-family: 'Lato';
	        src: url("{{ storage_path('fonts/Lato-Regular.ttf') }}") format('truetype');
	        font-weight: normal;
	        font-style: normal;
	    }
	    @font-face {
	        font-family: 'LatoBold';
	        src: url("{{ storage_path('fonts/Lato-Bold.ttf') }}") format('truetype');
	        font-weight: 600;
	        font-style: normal;
	    }

	    @font-face {
	        font-family: 'Maax';
	        src: url("{{ storage_path('fonts/Maax-Regular.ttf') }}") format('truetype');
	        font-weight: normal;
	        font-style: normal;
	    }
	    @font-face {
	        font-family: 'MaaxMedium';
	        src: url("{{ storage_path('fonts/Maax-Medium.ttf') }}") format('truetype');
	        font-weight: 500;
	        font-style: normal;
	    }
	    @font-face {
	        font-family: 'MaaxBold';
	        src: url("{{ storage_path('fonts/Maax-Bold.ttf') }}") format('truetype');
	        font-weight: 600;
	        font-style: normal;
	    }
        
	    @font-face {
	        font-family: 'Work Sans';
	        src: url("{{ storage_path('fonts/WorkSans-Regular.ttf') }}") format('truetype');
	        font-weight: normal;
	        font-style: normal;
	    }
	    @font-face {
	        font-family: 'Work Sans Medium';
	        src: url("{{ storage_path('fonts/WorkSans-Medium.ttf') }}") format('truetype');
	        font-weight: 500;
	        font-style: normal;
	    }
	    @font-face {
	        font-family: 'Work Sans SemiBold';
	        src: url("{{ storage_path('fonts/WorkSans-SemiBold.ttf') }}") format('truetype');
	        font-weight: 600;
	        font-style: normal;
	    }
	    @font-face {
	        font-family: 'Work Sans Bold';
	        src: url("{{ storage_path('fonts/WorkSans-Bold.ttf') }}") format('truetype');
	        font-weight: 700;
	        font-style: normal;
	    }

        @font-face {
	        font-family: 'Amiko';
	        src: url("{{ storage_path('fonts/Amiko-Regular.ttf') }}") format('truetype');
	        font-weight: normal;
	        font-style: normal;
	    }
	    @font-face {
	        font-family: 'Amiko Bold';
	        src: url("{{ storage_path('fonts/Amiko-Bold.ttf') }}") format('truetype');
	        font-weight: 600;
	        font-style: normal;
	    }
	    @font-face {
	        font-family: 'Amiko SemiBold';
	        src: url("{{ storage_path('fonts/Amiko-SemiBold.ttf') }}") format('truetype');
	        font-weight: 500;
	        font-style: normal;
	    }

	    html, body{ margin:0; padding:0; }
	    body{ 
	    	font-family: 'Work Sans', sans-serif;
	    	font-size:14px;
            font-weight: 400;
	    	line-height:20px;
	    	color:#000;
	    	position:relative;
	    	background-color: #fff;
	    }

	    strong{
	    	font-family: 'Lato', sans-serif; 
	    	font-weight: 600;
	    }

	    table{ font-size:14px; width:100%; margin:0; }

	    h1, h2, h3, h4{ 
	    	margin:0; 
	    	padding:0; 
	    	font-family: 'Lato', sans-serif; 
	    	font-weight: 500; 
	    	color:#343434; 
	    	letter-spacing: 1px;
	    }

	    h1{ 
	    	font-size:24px; 
	    	padding:0; 
	    	color:#333; 
	    }
	    h2{ 
	    	font-size:16px; 
	    	padding:0; 
	    	color:#A4A7B0; 
	    }
	    h3{ 
	    	padding-top:0px; 
	    	padding-bottom:5px; 
	    	margin-bottom:5px;
	    	font-size:14px;
            font-weight: bold;
            font-family: 'Amiko', sans-serif;
	    	border-bottom:3px solid #F3F3F3;
	    }

	    a{ color:#0075FF; text-decoration: none; }
        
		.page-break {
		    page-break-after: always;
		}

		#header{
			
			padding:0;
			margin:0;
		}
		#header .header-card{
			background-color: #F9FAFC;
			
			padding:40px 40px 0;
			margin:0;
		}

		#logo-icon{ width:80px; }
		#logo-text{ 
			width:calc(100% - 95px); 
			padding-left:15px;
		}
        #logo-text h1{
            font-family:'MaaxMedium', sans-serif; 
            font-size:40px;
            letter-spacing: -1px;
            color:#0075FF;
            padding:16px 0 5px;
        }
		.slogan{
			font-family: 'Amiko Bold', sans-serif; 
			color:#787878;
			letter-spacing: 2.2px;
            font-size:16px;
			line-height:1;
            text-transform: uppercase;
            padding:0;
		}
		.companyreg{
			color:#000;
			font-size:13px;
			line-height:16px;
		}
		.companyreg span{
			color:#0075FF;
			font-size:13px;
		}
		h2.document-meta-title{ 
            color:#333; 
            font-size:26px;
            font-weight: normal;
            padding-bottom:20px;
        }
		.document-meta-title,
		.document-number,
		.document-date{ 
            text-align:right; 
            font-family: 'Lato', sans-serif; 
            font-weight: normal;
        }
		.document-number,
		.document-date{ 
            text-align:right; 
            padding-bottom:0;
            margin:0;
            line-height:1;
            font-size:16px; 
	    	padding:0; 
	    	color:#A4A7B0;        
        }
		.document-total{
			font-family: 'LatoBold', sans-serif; 
			font-size:26px;
			color:#0075FF;
			padding:20px 0 0;
		}
		.document-payment-terms{
			font-family: 'Lato', sans-serif;
			font-size:14px;
            line-height:1;
			padding:9px 0 40px;
            color:#686868;
		}

		#body{
			
			padding:0;
		}
		#body .body-card{
			background-color: #fff;
			padding:40px;
		}

        
		#document-tasks{ margin-top:25px; margin-bottom:0; }
		#document-tasks th{
            font-family: 'Amiko', sans-serif;
			font-size:13px;
			color: #000;
			vertical-align: top;
			line-height:20px;
			padding:0 0 10px;
            border-bottom:1px solid #000;
		}
		#document-tasks td{
            font-family: 'Work Sans', sans-serif;
			font-size:12px;
            color:#000;
			line-height:1;
			padding:10px 0 13px 0;
			vertical-align: top;
		}
		/*#document-tasks tr{ background-color: #F7F7F7; }
		#document-tasks tr:nth-child(2n+2){ background-color: #FFF; }*/
		.col-hours{ width:8%; text-align:left; }
		.col-rate{ width:8%; text-align:left; }
		.col-total{ width:14%; text-align:right; position: relative; }
        
		
        #document-tasks tr.table-total{  }
		#document-tasks tr.table-total td,
		#document-tasks tr.table-total td h3{ font-size:13px; font-weight:bold; color:#000; margin-top:1px; }
		#document-tasks tr.table-total td{ padding:13px 0 10px 0; border-top:1px solid #000; }

		.subtotal h3,
		.table-total h3{ font-size:12px;  padding:0; border:0; margin:0; }
        
        .subtotal .col-total-label{  }
        .subtotal .col-total-label h3{ font-size:12px; width:100%; display:inline-block; vertical-align: middle; text-align:right; padding:0; border:0; margin:0; }
         
        .table-total .col-total-label h3{ 
            font-size:14px; 
            width:100%; 
            display:inline-block; 
            vertical-align: middle; 
            text-align:right; 
            padding:0; 
            border:0; 
        }
        .table-total .col-total span{ color:#000; font-size:14px;  }

        
		#body table{
			font-family: 'Work Sans', sans-serif;
		}

		.divblock{
            font-family: 'Work Sans', sans-serif;
			font-size:13px;
			line-height:normal;
			padding-bottom:0px;
            color:#333;
		}
		.divblock h3{
            font-family: 'Amiko', sans-serif;
			font-size:13px;
            letter-spacing: 0;
            text-transform: none;
            color:#000;
		}
        
		#footer{
			background-color: #fff;
			padding:0;
		}
		#footer .footer-card{
			background-color: #fff;
			padding:0 40px 40px;
			
		}
        #foot-details .divblock{
            font-size:13px
        }
        .footer-blurp{
            font-style:italic;
            color:#0075FF;
            font-size:16px;
        }

	</style>

    @php $subtotal = 0; @endphp
	@foreach($data['tasks'] as $task)
		@if($task->estimate_time != 'Heading')
			@php 
			$tasktotal = ($task->estimate_time * $data['project']->rate);
			$subtotal = $subtotal + $tasktotal; 
			@endphp
		@endif
    @endforeach
	<div id='header'>
		<div class='header-card'>
		  	<table width="100%" border="0" cellpadding="0" cellspacing="0">
				<tr>
					<td width='70%' valign='top'>
						<table width="100%" border="0" cellpadding="0" cellspacing="0">
							<tr>
								<td id='logo-icon' valign='top'>
									@php $imageurl = App\Settings::get_setting('company_logo'); @endphp
									<img src="{{ $imageurl }}" width="80">
								</td>
								<td id='logo-text' valign='middle'>
									<h1>BitBrighter (Pty) Ltd.</h1>
									<div class='slogan'>Online Design and Development</div>
									<!--<div class='companyreg'><span>co. registraion:</span> 2018/496083/07</div>-->
								</td>
							</tr>
						</table>
					</td>
					<td width='30%' valign='top' align="right">
						<h2 class='document-meta-title'>QUOTATION</h2>
						<h2 class='document-number'>#{{ $data['quote_id'] }}</h2>
						<h2 class='document-date'>{{ date('F dS, Y', $data['timestamp']) }}</h2>
					</td>
				</tr>
				<tr>
					<td width='70%' valign='top'>
						<div class='document-payment-terms'>
						- 50% deposit & finalized content required to start design or development.<br>
						- Final deposit required when the project is completed.
						</div>
					</td>
					<td width='30%' valign='top' align="right">
						<div class='document-total'>R {{ number_format($subtotal, 2, '.', ' ') }}</div>
					</td>
				</tr>
			</table>
		</div>
	</div>
	<div id='body'>
		<div class='body-card'>

			<table id='project-job' width="100%" style="width:100%" border="0">
				<tr>
					<td valign='top'>
						<div class='divblock'>
							<h3>Project / Job</h3>
							<div>{{ $data['project']->name }}: {{ $data['project']->brief }}</div>
						</div>
					</td>
				</tr>
			</table>

			
			<table id='document-tasks' width="100%" border="0" cellpadding="0" cellspacing="0">
				<tr>
					<th>DESCRIPTION</th>
					<th class='col-hours'>HOURS</th>
					<th class='col-rate'>RATE</th>
					<th class='col-total'>TOTAL</th>
				</tr>
				@foreach($data['tasks'] as $task)
				@if($task->estimate_time == 'Heading')
					<tr>
						<td><strong>{{ $task->brief }}</strong></td>
						<td class='col-hours'></td>
						<td class='col-rate'></td>
						<td class='col-total'></td>
					</tr>
				@else
					<tr>
						<td>{{ $task->brief }}</td>
						<td class='col-hours'>{{ $task->estimate_time }}</td>
						<td class='col-rate'>{{ $data['project']->rate }}</td>
						@php $tasktotal = ($task->estimate_time * $data['project']->rate); @endphp
						<td class='col-total'><span class='currency'>R</span>{{ number_format($tasktotal, 2, '.', ' ') }}</td>
					</tr>
				@endif
				@endforeach
				<!--<tr class='subtotal'>
					<td ></td>
					<td colspan='2' align='right'><h3>SUBTOTAL:</h3></td>
					<td class='col-total'><span class='currency'>R</span> {{ number_format($subtotal, 2) }}</td>
				</tr>
				<tr class='subtotal'>
					<td ></td>
					<td colspan='2' class='col-total'><h3>LESS PAYMENT:</h3></td>
					<td class='col-total'><span class='currency'>R</span> - </td>
				</tr>-->
				<tr class='table-total'>
					<td colspan='2'></td>
					<td align='right'><h3>BALANCE:</h3></td>
					<td class='col-total'><span class='currency'>R</span> {{ number_format($subtotal, 2, '.', ' ') }}</td>
				</tr>
			</table>
			
		</div>
	</div>
    
	<div id='footer'>
		<div class='footer-card'>

			<table id="foot-details" width="100%" border="0">
				<tr>
					<td width='28.5%' valign='top'>
						<div class='divblock'>
							<h3>Banking Details</h3>
							<div><strong>Account Name:</strong> <span>{{ App\Settings::get_setting('account_owner') }}</span></div>
							<div><strong>Bank:</strong> <span>{{ App\Settings::get_setting('bank_name') }}</span></div>
							<div><strong>Account Type:</strong> <span>{{ App\Settings::get_setting('account_type') }}</span></div>
							<div><strong>Account No:</strong> <span>{{ App\Settings::get_setting('account_number') }}</span></div>
							<div><strong>Branch Code:</strong> <span>{{ App\Settings::get_setting('branch_code') }}</span></div>
						</div>
					</td>
					<td width='5%' valign='top'>
					<td width='28.5%' valign='top'>
						<div class='divblock'>
							<h3>From</h3>
							<div><strong>{{ App\Settings::get_setting('company_name') }}</strong> <span></span></div>
							<div>{{ App\Settings::get_setting('company_address_1') }}, {{ App\Settings::get_setting('company_address_2') }}</div>
							<div>{{ App\Settings::get_setting('suburb') }}, {{ App\Settings::get_setting('city') }}</div>
							<div>{{ App\Settings::get_setting('area_code') }}</div>
							<div>{{ App\Settings::get_setting('company_tel') }}</div>
							<div><a href="mailto:{{ App\Settings::get_setting('company_email') }}">{{ App\Settings::get_setting('company_email') }}</a></div>
						</div>
					</td>
					<td width='5%' valign='top'>
					<td width='28.5%' valign='top'>
						<div class='divblock'>
							<h3>For</h3>
                            <div><strong>{{ $data['client']->company }}</strong> <span></span></div>
							<div>{{ $data['client']->extra }}</div>
							<div><a href="mailto:{{ $data['client']->email }}">{{ $data['client']->email }}</a></div>
						</div>
					</td>
				</tr>
		    </table>
		</div>
        
		<div class='footer-card'>
            <div class='footer-blurp'>
                <p style="text-align: center; ">{{ $data['content'] }}</p>
            </div>
		</div>
	</div>
	
</body>
</html>