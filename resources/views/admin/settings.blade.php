@extends('layouts.app')

@section('content')

    <div class="card">
        
        <div class="card-header hasicon">
            <div class='card-header-icon svg-icon'>@include('icons.settings')</div>
            <div class='card-header-text'>Administrator Settings</div>
        </div>

        <div class="card-body">

            @if (session('status'))
                <div class="alert alert-success alert-dismissible fade show" role="alert">
                    {!! session('status') !!}
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            @endif

            <div class="imageupload">

                @php 
                $imageurl = App\Settings::get_setting('company_logo');
                @endphp
                <div class="form-group row">
                    <label for="company_logo" class="col-md-2 col-form-label text-md-right">{{ __('Company Logo:') }}</label>

                    <!--<div class="col-md-7">
                        
                            @csrf

                            <input id="company_logo" type="text" class="form-control @error('company_logo') is-invalid @enderror" name="company_logo" value="{{ url($imageurl) }}" autocomplete="company_logo" autofocus>
                        </form>
                    </div>-->
                    <div class="col-md-3">
                       
                        <form id="dropzone" action="{{ route('image.upload.store') }}" class="dropzone">
                             @csrf
                            <div class="dz-message"  @if($imageurl) style='background-image:url({{ url($imageurl) }});' @endif>
                                <img src="{{ url($imageurl) }}" width="100%" style="opacity:0;">
                                <div class='dz-message-text'>
                                    Drop files here or click to upload 
                                    <span class="note">.svg, .jpg, .png, .gif</span>
                                </div>
                            </div>
                        </form>
                       
                    </div>
                </div>

                
                <script type="text/javascript">
                    Dropzone.options.dropzone =
                    {
                        maxFilesize: 12,
                        renameFile: function(file) {
                            var dt = new Date();
                            var time = dt.getTime();
                            return time+file.name;
                        },
                        acceptedFiles: ".svg,.jpeg,.jpg,.png,.gif",
                        addRemoveLinks: true,
                        timeout: 50000,
                        removedfile: function(file) 
                        {
                            var name = file.upload.filename;

                            $.ajax({
                                headers: {
                                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                                },
                                type: 'POST',
                                url: '{{ url("image/delete") }}',
                                data: {filename: name},
                                success: function (data){
                                    console.log("File has been successfully removed!!");
                                },
                                error: function(e) {
                                    console.log(e);
                                }
                            });

                            var fileRef;

                            return (fileRef = file.previewElement) != null ? fileRef.parentNode.removeChild(file.previewElement) : void 0;
                        },

                        success: function(file, response) 
                        {
                            console.log(response);
                        },
                        error: function(file, response)
                        {
                            return false;
                        }
                    };
                </script>

            </div>

            <form action="{{ route('admin.settings.update') }} " method="post" enctype="multipart/form-data">
                @csrf

                <div class="form-group row">
                    <label for="company_name" class="col-md-2 col-form-label text-md-right">{{ __('Company Name:') }}</label>

                    <div class="col-md-10">
                        <input id="company_name" type="text" class="form-control @error('company_name') is-invalid @enderror" name="company_name" value="{{ App\Settings::get_setting('company_name') }}" autocomplete="company_name" autofocus>

                        @error('company_name')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                </div>

                <div class="form-group row">
                    <label for="company_email" class="col-md-2 col-form-label text-md-right">{{ __('Company Email:') }}</label>

                    <div class="col-md-10">
                        <input id="company_email" type="text" class="form-control @error('company_email') is-invalid @enderror" name="company_email" value="{{ App\Settings::get_setting('company_email') }}" autocomplete="company_email" autofocus>

                        @error('company_email')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                </div>

                <div class="form-group row">
                    <label for="company_tel" class="col-md-2 col-form-label text-md-right">{{ __('Company Telephone:') }}</label>

                    <div class="col-md-10">
                        <input id="company_tel" type="text" class="form-control @error('company_tel') is-invalid @enderror" name="company_tel" value="{{ App\Settings::get_setting('company_tel') }}" autocomplete="company_tel" autofocus>

                        @error('company_tel')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                </div>

                <div class="form-group row">
                    <label for="company_rate" class="col-md-2 col-form-label text-md-right">{{ __('Hourly Rate:') }}</label>

                    <div class="col-md-10">
                        <input id="company_rate" type="text" class="form-control @error('company_rate') is-invalid @enderror" name="company_rate" value="{{ App\Settings::get_setting('company_rate') }}" autocomplete="company_rate" autofocus>

                        @error('company_rate')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                </div>
                <div class="form-group row">
                    <label for="company_quote_start" class="col-md-2 col-form-label text-md-right">{{ __('Quote Start Index:') }}</label>

                    <div class="col-md-10">
                        <input id="company_quote_start" type="text" class="form-control @error('company_quote_start') is-invalid @enderror" name="company_quote_start" value="{{ App\Settings::get_setting('company_quote_start') }}" autocomplete="company_quote_start" autofocus>

                        @error('company_quote_start')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                </div>
                <div class="form-group row">
                    <label for="company_invoice_start" class="col-md-2 col-form-label text-md-right">{{ __('Invoice Start Index:') }}</label>

                    <div class="col-md-10">
                        <input id="company_invoice_start" type="text" class="form-control @error('company_invoice_start') is-invalid @enderror" name="company_invoice_start" value="{{ App\Settings::get_setting('company_invoice_start') }}" autocomplete="company_invoice_start" autofocus>

                        @error('company_invoice_start')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                </div>

                <div class="form-group row">
                    <label for="company_address_1" class="col-md-2 col-form-label text-md-right">{{ __('Company Address:') }}</label>

                    <div class="col-md-10">
                        <div class='input-group-item'>
                            <input id="company_address_1" type="text" class="form-control @error('company_address_1') is-invalid @enderror" name="company_address_1" value="{{ App\Settings::get_setting('company_address_1') }}" placeholder="Unit & building name" autocomplete="company_address_1" autofocus>
                            @error('company_address_1')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                        <div class='input-group-item'>
                            <input id="company_address_2" type="text" class="form-control @error('company_address_2') is-invalid @enderror" name="company_address_2" value="{{ App\Settings::get_setting('company_address_2') }}" placeholder="Street number & name" autocomplete="company_address_2" autofocus>
                            @error('company_address_2')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                        <div class='input-group-item'>
                            <input id="suburb" type="text" class="form-control @error('suburb') is-invalid @enderror" name="suburb" value="{{ App\Settings::get_setting('suburb') }}" placeholder="Suburb" autocomplete="suburb" autofocus>
                            @error('suburb')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                        <div class='input-group-item'>
                            <input id="city" type="text" class="form-control @error('city') is-invalid @enderror" name="city" value="{{ App\Settings::get_setting('city') }}" placeholder="City or town" autocomplete="city" autofocus>
                            @error('city')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                        <div class='input-group-item'>
                            <input id="area_code" type="text" class="form-control @error('area_code') is-invalid @enderror" name="area_code" value="{{ App\Settings::get_setting('area_code') }}" placeholder="Area Code" autocomplete="area_code" autofocus>
                            @error('area_code')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                    </div>
                </div>

                <div class="form-group row">
                    <label for="company_address_1" class="col-md-2 col-form-label text-md-right">{{ __('Bank Account Details:') }}</label>

                    <div class="col-md-10">
                        <div class='input-group-item'>
                            <input id="account_owner" type="text" class="form-control @error('account_owner') is-invalid @enderror" name="account_owner" value="{{ App\Settings::get_setting('account_owner') }}" placeholder="Account holder's name" autocomplete="account_owner" autofocus>
                            @error('account_owner')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                        <div class='input-group-item'>
                            <input id="bank_name" type="text" class="form-control @error('bank_name') is-invalid @enderror" name="bank_name" value="{{ App\Settings::get_setting('bank_name') }}" placeholder="Bank name" autocomplete="bank_name" autofocus>
                            @error('bank_name')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>

                        @php $account_type = App\Settings::get_setting('account_type'); @endphp
                        <div class='input-group-item'>
                            <select id="account_type" class="form-control @error('account_type') is-invalid @enderror" name="account_type" autofocus>
                                <option value=''>Select an account type</option>
                                <option value='Cheque Account' @if($account_type == 'Cheque Account') selected="selected" @endif>Cheque Account</option>
                                <option value='Savings Account' @if($account_type == 'Savings Account') selected="selected" @endif>Savings Account</option>
                                <option value='Business Account' @if($account_type == 'Business Account') selected="selected" @endif>Business Account</option>
                            </select>
                            @error('suburb')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                        <div class='input-group-item'>
                            <input id="account_number" type="text" class="form-control @error('account_number') is-invalid @enderror" name="account_number" value="{{ App\Settings::get_setting('account_number') }}" placeholder="Account number" autocomplete="account_number" autofocus>
                            @error('city')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                        <div class='input-group-item'>
                            <input id="branch_code" type="text" class="form-control @error('branch_code') is-invalid @enderror" name="branch_code" value="{{ App\Settings::get_setting('branch_code') }}" placeholder="Branch Code" autocomplete="branch_code" autofocus>
                            @error('branch_code')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                    </div>
                </div>

                <!--<div class="form-group row">
                    <label for="company_logo" class="col-md-2 col-form-label text-md-right">{{ __('Company Logo:') }}</label>

                    <div class="col-md-10">

                        <div class="dz-message needsclick">
                            Drop files here or click to upload.<br>
                            <span class="note needsclick">(Only svg and jpg/jpeg file types accepted.)</span>
                        </div>

                    </div>
                </div>-->

                <div class="form-group row mb-0">
                    <div class="col-md-10 offset-md-2">
                        <button type="submit" class="btn btn-primary">
                            {{ __('Update') }}
                        </button>
                    </div>
                </div>

            </form>

        </div>

    </div>

@endsection
