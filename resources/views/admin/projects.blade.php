@extends('layouts.app')

@section('content')

    <div class="card projects">
        
        <div class="card-header hasicon">
            <div class='card-header-icon svg-icon'>@include('icons.projects')</div>
            <div class='card-header-text'>Administrator Projects</div>
            <div class='card-header-button'>
                <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#addProjectModal">Create New Project</button>
            </div>
        </div>

        <div class="card-body">
            
            
            
            @if (session('status'))
                <div class="alert alert-success alert-dismissible fade show" role="alert">
                    {!! session('status') !!}
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            @endif

            @if(isset($projects) && count($projects) > 0)
            
                <div id='project-filters' class='filters'>
                    @include('admin.projects.statusfilter')
                    @include('admin.projects.projectsorderbyfilter')
                </div>

                <div class='project-cards'>
                    @foreach($projects as $project)
                    @php
                    $pclient = Auth::user()->find($project->user_id);

                    $tasks = App\Tasks::where('project_id', $project->id)->get();
                    $taskcount = ( isset($tasks) )? count($tasks): 0;
                    $hours = 0;
                    if($taskcount){
                        foreach($tasks as $task){
                            if($task->estimate_time != 'Heading'){
                                $hours += $task->estimate_time;
                            }
                        }
                    }
                    $project_total = ($hours * $project->rate);
                    @endphp

                    <div class='project-card {{ $project->status }}-card'>
                        <div class='project-card-name'><h2><a href="{{ route('admin.projects.edit', $project->id) }}">{{ $project->name }}</a> <div class='project-card-total'>R {{ number_format($project->total, 2) }}</div></h2></div>
                        <a href="{{ route('admin.client.projects', $project->user_id) }}" class='project-card-client'>{{ $pclient->name }} </a>
                        <div class='project-card-status {{ $project->status }}'>{{ $project->status }}</div>
                        <div class='project-card-details'>
                            <div class='project-card-taskcount'><strong>Tasks:</strong> {{ $taskcount }}</div>
                            <div class='project-card-duedate'><strong>Due:</strong> {{ date('Y-m-d', strtotime($project->due_date)) }}</div>
                            <div class='project-card-rate'><strong>Rate:</strong> R {{ $project->rate }} ph</div>
                            <div class='project-card-taskhours'><strong>Hours:</strong> {{ $hours }} hrs</div>
                            <div class='project-card-brief'>{{ $project->brief }}</div>
                        </div>
                    </div>
                    @endforeach
                </div>
            @else 
                <p><em>No projects have been created. <a href="#" data-toggle="modal" data-target="#addProjectModal">Create New Project ?</a></em></p>
            @endif
        </div>

    </div>

    @include('modals.projects.add')

@endsection
