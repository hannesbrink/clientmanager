<div id='container'>
    
    <header class="grid-item header">Header</header>
    
    <h1 class="grid-item title">Page Title</h1>
    
    <main class="grid-item main">
		@yield('content')
    </main>
    
    <aside class="grid-item sidebar">Main Content</aside>
    
    <footer class="grid-item footer">Footer</footer>
</div>