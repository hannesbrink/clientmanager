@extends('layouts.app')

@section('content')

    <div class="card">
        
        <div class="card-header hasicon">
            <div class='card-header-icon svg-icon'>@include('icons.clients')</div>
            <div class='card-header-text'>Admin Clients</div>
        </div>

        <div class="card-body">
            @if (session('status'))
                <div class="alert alert-success alert-dismissible fade show" role="alert">
                    {!! session('status') !!}
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            @endif

            <form action="{{ route('admin.client.update', $client->id ) }} " method="post" enctype="multipart/form-data">
                @csrf

                <div class="form-group row">
                    <label for="client_status" class="col-md-2 col-form-label text-md-right">{{ __('Status:') }}</label>
                    <div class="col-md-10">
                        <div class='input-group-item'>
                            <select id="client_status" class="form-control @error('client_status') is-invalid @enderror" name="client_status" autofocus>
                                <option value=''>Select client status</option>
                                <option value='active' @if($client->status == 'active') selected="selected" @endif>Active</option>
                                <option value='inactive' @if($client->statuss == 'inactive') selected="selected" @endif>Suspended</option>
                                <option value='bad' @if($client->status == 'bad') selected="selected" @endif>Bad Client</option>
                            </select>
                            @error('suburb')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                    </div>
                </div>
                
                <div class="form-group row">
                    <label for="client_name" class="col-md-2 col-form-label text-md-right">{{ __('Name:') }}</label>
                    <div class="col-md-10">
                        <input id="client_name" type="text" class="form-control @error('client_name') is-invalid @enderror" name="client_name" value="{{ $client->name }}" autocomplete="client_name" autofocus>

                        @error('client_name')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                </div>

                <div class="form-group row">
                    <label for="client_company" class="col-md-2 col-form-label text-md-right">{{ __('Company:') }}</label>
                    <div class="col-md-10">
                        <input id="client_company" type="text" class="form-control @error('client_company') is-invalid @enderror" name="client_company" value="{{ $client->company }}" autocomplete="client_name" autofocus>

                        @error('client_company')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                </div>

                <div class="form-group row">
                    <label for="client_email" class="col-md-2 col-form-label text-md-right">{{ __('Email:') }}</label>
                    <div class="col-md-10">
                        <input id="client_email" type="text" class="form-control @error('client_email') is-invalid @enderror" name="client_email" value="{{ $client->email }}" autocomplete="client_email" autofocus>

                        @error('client_email')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                </div>

                <div class="form-group row bbclientcolor_container">
                    <label for="client_color" class="col-md-2 col-form-label text-md-right">{{ __('Color:') }}</label>
                    <div class="col-md-10">
                        <input id="client_color" type="text" class="form-control bbcolorpicker @error('client_color') is-invalid @enderror" name="client_color" value="{{ $client->color }}" autocomplete="client_color" style="border: 3px solid {{ $client->color }};" autofocus>
                        @error('client_color')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                </div>

               <div class="form-group row">
                    <label for="client_extra" class="col-md-2 col-form-label text-md-right">{{ __('Additional Information:') }}</label>
                    <div class="col-md-10">
                        <textarea row="3" id="client_extra" class="form-control @error('client_extra') is-invalid @enderror" name="client_extra" autocomplete="client_extra" autofocus>{{ $client->extra }}</textarea>
                        @error('client_extra')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                </div>

                <div class="form-group row mb-0">
                    <div class="col-md-10 offset-md-2">
                        <button type="submit" class="btn btn-primary">
                            {{ __('Update') }}
                        </button>
                    </div>
                </div>

            </form>

        </div>

    </div>

@endsection
