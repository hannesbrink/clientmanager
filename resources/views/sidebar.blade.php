

    <div class="card navigation">
        
        <div class="card-header" data-toggle="collapse" data-target="#mainmenu" aria-expanded="true" aria-controls="mainmenu">
            Navigation
            <div class='chevron animate'><svg aria-hidden="true" focusable="false" data-prefix="fal" data-icon="chevron-up" class="svg-inline--fa fa-chevron-up fa-w-14" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512"><path fill="currentColor" d="M4.465 366.475l7.07 7.071c4.686 4.686 12.284 4.686 16.971 0L224 178.053l195.494 195.493c4.686 4.686 12.284 4.686 16.971 0l7.07-7.071c4.686-4.686 4.686-12.284 0-16.97l-211.05-211.051c-4.686-4.686-12.284-4.686-16.971 0L4.465 349.505c-4.687 4.686-4.687 12.284 0 16.97z"></path></svg></div>
        </div>

        <div id="mainmenu" class="card-body collapse show">
            

            <ul class="menu-list">

               
                <li class="menu-list-item animate">
                    <span class='svg-icon'>@include('icons.dashboard')</span>
                    <a href='{{ route("dashboard") }}' class='animate'>Dashboard</a>
                </li>

                @if(Auth::user() && Auth::user()->type == 'admin')
                    <li class="menu-list-item animate">
                        <span class='svg-icon'>@include('icons.settings')</span>
                        <a href='{{ route("admin.settings") }}' class='animate'>Settings</a>
                    </li>
                    <li class="menu-list-item animate">
                        <span class='svg-icon'>@include('icons.clients')</span>
                        <a href='{{ route("admin.clients") }}' class='animate'>Clients</a>
                    </li>
                @else
                
                    @php 
                    $route = route("profile");
                    if( strpos( Request::url(), 'admin' ) !== false ){
                        $route = route("admin.profile");
                    }
                    @endphp
                    <li class="menu-list-item animate">
                        <span class='svg-icon'>@include('icons.profile')</span>
                        <a href='{{ $route }}' class='animate'>My Profile</a>
                    </li>

                @endif

                    @php 
                    $route = route("projects");
                    if( strpos( Request::url(), 'admin' ) !== false ){
                        $route = route("admin.projects");
                    }
                    @endphp
                    <li class="menu-list-item animate">
                        <span class='svg-icon'>@include('icons.projects')</span>
                        <a href='{{ $route }}' class='animate'>Projects</a>
                    </li>


                    <li class="menu-list-item animate">
                        <span class='svg-icon'>@include('icons.logout')</span>
                        <a href="{{ route('logout') }}" class='animate' onclick="event.preventDefault(); document.getElementById('logout-form').submit();">{{ __('Logout') }}</a>
                    </li>

            </ul>
        </div>

    </div>

