<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


// public routes
Route::get('/', function () {
    return view('welcome');
});
Route::get('captcha-form', 'CaptchaController@captchForm');
Route::post('store-captcha-form', 'CaptchaController@storeCaptchaForm');

Route::post('image/upload/store', 'FileUploadController@companyLogoStore')->name('image.upload.store');
Route::post('image/delete', 'FileUploadController@companyLogoDestroy')->name('image.delete');


// user routes 
Auth::routes(); //['verify' => true]

Route::get('/dashboard', 'HomeController@index')->middleware('verified')->name('dashboard');

Route::get('/projects', 'HomeController@projects')->middleware('verified')->name('projects');
Route::post('/projects/add', 'HomeController@projectsAdd')->middleware('verified')->name('projects.add');
Route::get('/projects/edit/{id}', 'HomeController@projectsEdit')->middleware('verified')->name('projects.edit');
Route::post('/projects/update/{id}', 'HomeController@projectsUpdate')->middleware('verified')->name('projects.update');

Route::get('/profile', 'HomeController@profile')->middleware('verified')->name('profile');
Route::post('/profile/update/{id}', 'HomeController@profileUpdate')->middleware('verified')->name('profile.update');


// admin routes
Route::get('/admin', 'AdminController@admin')->middleware('is_admin')->name('admin');
Route::get('/admin/settings', 'AdminController@settings')->middleware('is_admin')->name('admin.settings');
Route::post('/admin/settings/update', 'AdminController@settingsUpdate')->middleware('is_admin')->name('admin.settings.update');
Route::get('/admin/clients', 'AdminController@clients')->middleware('is_admin')->name('admin.clients');
Route::get('/admin/clients/edit/{id}', 'AdminController@clientEdit')->middleware('is_admin')->name('admin.client.edit');
Route::post('/admin/clients/update/{id}', 'AdminController@clientUpdate')->middleware('is_admin')->name('admin.client.update');
Route::get('/admin/clients/delete/{id}', 'AdminController@clientDelete')->middleware('is_admin')->name('admin.client.delete');
Route::get('/admin/projects', 'AdminController@projects')->middleware('is_admin')->name('admin.projects');
Route::get('/admin/projects/filter/total', 'AdminController@filterProjectsByTotal')->middleware('is_admin')->name('admin.filter.projects.total');
Route::get('/admin/projects/{id}', 'AdminController@clientProjects')->middleware('is_admin')->name('admin.client.projects');
Route::post('/admin/projects/add', 'AdminController@projectsAdd')->middleware('verified')->name('admin.projects.add');
Route::get('/admin/projects/edit/{id}', 'AdminController@projectsEdit')->middleware('verified')->name('admin.projects.edit');
Route::post('/admin/projects/update/{id}', 'AdminController@projectsUpdate')->middleware('verified')->name('admin.projects.update');
Route::post('/admin/projects/{id}/delete', 'AdminController@projectsDelete')->middleware('verified')->name('admin.projects.delete');

//Route::post('/admin/projects/{id}/taskadd', 'AdminController@projectsTaskAdd')->middleware('verified')->name('admin.projects.task.add');
Route::post('/admin/projects/taskaddjson', 'AdminController@projectsTaskAddJson')->middleware('verified')->name('admin.projects.task.addjson');
Route::post('/admin/task/delete', 'AdminController@taskDelete')->middleware('verified')->name('admin.task.delete');
Route::post('/admin/projects/tasksortjson', 'AdminController@projectsTaskSortJson')->middleware('verified')->name('admin.projects.task.sortjson');

Route::get('/admin/projects/{id}/quote/{view_type}','AdminController@projectsQuotePDF')->middleware('verified')->name('admin.projects.quote');
Route::get('/admin/projects/quote/{id}/{view_type}','AdminController@projectsQuoteOpen')->middleware('verified')->name('admin.projects.quote.open');
Route::get('/admin/projects/{id}/invoice/{view_type}','AdminController@projectsInvoicePDF')->middleware('verified')->name('admin.projects.invoice');
Route::get('/admin/projects/invoice/{id}/{view_type}','AdminController@projectsInvoiceOpen')->middleware('verified')->name('admin.projects.invoice.open');

Route::post('/admin/projects/quote/delete/{id}','AdminController@projectsQuoteDelete')->middleware('verified')->name('admin.projects.quote.delete');
Route::post('/admin/projects/invoice/delete/{id}','AdminController@projectsInvoiceDelete')->middleware('verified')->name('admin.projects.invoice.delete');

Route::get('/admin/profile', 'AdminController@profile')->middleware('is_admin')->name('admin.profile');
