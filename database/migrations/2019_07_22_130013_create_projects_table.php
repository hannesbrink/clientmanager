<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProjectsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('projects', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->longText('brief');
            $table->string('estimated_time');
            $table->string('lapsed_time');
            $table->string('status')->default('quote');
            $table->timestamp('due_date')->nullable();
            $table->integer('rate');
            $table->double('total', 15, 8)->default(0);
            $table->double('paid_amount', 15, 8)->default(0);
            $table->integer('user_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('projects');
    }
}
