(function($){

    $(window).bind("load", function() {

        console.log('site completed loaded.');
        $('textarea').each(function(){
            autosize(this);
        });

    });

    $(document).ready(function(){

        console.log('custom jquery ready');
        $('.bbcolorpicker').colorpicker({
            container: '.bbclientcolor_container',
            format: 'hex',
            align: 'left'
        });


    });

})(jQuery);
