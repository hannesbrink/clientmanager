(function ($) {

    $(window).bind("load", function () {

        console.log('admin window loaded');

        autosize($('#client_extra'));
        
        var group = $("ol.vertical").sortable({
            group: 'serialization',
            pullPlaceholder: false,
            onDragStart: function($item, container, _super) {
                
            },
            onDrop: function($item, container, _super) {

                console.log( $item.index() );
                
                var ajaxurl = $("#sorttask_url").val();
                var projectid = $('#projectid').val();
                
                var savedata = new Array();
                
                var ol = $item.parent();
                var lis = ol.find('li');
                for(var i=0; i<lis.length; i++){
                    $(lis[i]).find('.project-task').attr('data-sortid', i);
                    $(lis[i]).find('.project-task').attr('data-sortid', i);
                    savedata[i] = $(lis[i]).find('.project-task').attr('data-taskid');
                }

                console.log(savedata);
                
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });

                $.ajax({
                    type: 'POST',
                    url: ajaxurl,
                    data: {
                        order: JSON.stringify(savedata),
                    },
                    success: function (response) {
                        console.log(response);
                    }
                });
                
            }            
        });

        $('textarea.task-textarea').each(function () {
            autosize(this);
        }).on('autosize:update', function () {
            updateTask($(this));
        });

    });

    $(document).ready(function () {

        console.log('admin document ready');

        $('[data-toggle="tooltip"]').tooltip()
        
        $('.bbcolorpicker').colorpicker({
            container: '.bbclientcolor_container',
            format: 'hex',
            align: 'left'
        });

        $('select[name=project_status]').on('change', function(){
            var selected = $(this).find('option:selected').val();
            if(selected == 'unpaid'){
                $('.unpaid-amount').addClass('active');
            } else {
                $('.unpaid-amount').removeClass('active');
                $('#project_paid_amount').val(0.00);
            }
        });
        
        $('.generatepdf').on('click', function (e) {
            setTimeout( function(){ 
                window.location.href = window.location.href;
            }, 500);
        });
        
        $('.btn-danger').on('click', function (e) {
            if (!confirm("Are you sure you want to delete?")) {
                e.preventDefault();
            }
        });

        $(document).on('keydown', 'textarea.task-textarea', function (event) {

            var textarea = $(this);
            console.log(event.keyCode);
            if (event.keyCode === 10 || event.keyCode === 13) {
                event.preventDefault();
                var html = $("#empty-task-entry").html();
                textarea.parent().parent().parent().parent().parent().find('ol').append(html).find('textarea').focus();
            } else if (event.keyCode === 8) {
                console.log('_'+$.trim(textarea.val())+"_");
                var textval = $.trim(textarea.val());
                if (textval == '') {
                    
                    var count = textarea.parent().parent().parent().parent().parent().find('ol li').length;
                    console.log('textarea empty remove task ');
                    
                    if (count > 1) {
                        textarea.parent().parent().parent().remove();
                        //$('.project-tasks .project-task:last-of-type .project-task-desc textarea').focus();
                        
                        removeTask(textarea);
                        event.preventDefault();
                    }
                } else {
                    console.log('textarea has text');
                    updateTask(textarea);
                }
            }


        });

        $(document).on('blur', 'textarea.task-textarea', function (e) {
            var textarea = $(this);
            if (!$.trim(textarea.val())) {
                console.log('focus lost on empty textarea do nothing');
            } else {
                console.log('focus lost on populated textarea');
                updateTask(textarea);
            }
        });

        $(document).on('change', '.task-time', function (e){
            console.log( $(this).find('option:selected').val() );
            var textarea = $(this).parent().parent().find('textarea.task-textarea');
            updateTask(textarea);
        });
        $(document).on('click', '.project-task-icon', function(){
            
            var parent = $(this).parent();
            var textarea = $(this).parent().find('textarea.task-textarea');
            var statusinput = $(this).find('.task-status').val();
            if(statusinput == 'incomplete'){
                parent.removeClass('incomplete').addClass('completed');
                $(this).find('.task-status').val('completed');
            } else {
                parent.removeClass('completed').addClass('incomplete');
                $(this).find('.task-status').val('incomplete');
            }
            updateTask(textarea);
            
        })

        $('.nav.nav-tabs li a').on('click', function(e){
            e.preventDefault();
            
            $('.nav.nav-tabs li').removeClass('active');
            $(this).parent().addClass('active');
            
            var target = $(this).attr('data-target');
            $('.project-menu-page').hide();
            
            $(target).slideDown('fast');
            
            
        });
        
        $('#project-status-filter').on('change', function(){
            var val = $(this).find('option:selected').val();
            if(val == 'all'){
                
                $('.project-card').show();
                
            } else {
                
                var show = val+'-card';
                console.log(show); 
                var cards = $('.project-card');
                console.log(cards.length);
                for(var i=0; i<cards.length; i++){
                    console.log(cards.eq(i));
                    if(!cards.eq(i).hasClass(show)){
                        cards.eq(i).hide();
                    } else {
                        cards.eq(i).show()
                    }
                }
                
            }
        });
        
        $('#orderby-filter').on('change', function(){
            window.location.href = $(this).find('option:selected').val();
        });
        
    });

    function updateTask(textarea) {

        var ajaxurl = $("#addtask_url").val();
        var projectid = $('#projectid').val();
        
        var tasktime = textarea.parent().parent().find('.project-task-time select.task-time option:selected').val();
        var taskstatus = textarea.parent().parent().find('.project-task-icon .task-status').val();
        var taskdesc = textarea.val();
        var sortid = textarea.parent().parent().attr('data-sortid');
        if (sortid == null) {
            sortid = $('#project-tasks ol li').length - 1;
        }
        
        var taskid = textarea.parent().parent().attr('data-taskid');
        if (taskid == null) {
            taskid = 0;
        }
        
        if(taskstatus == null){
            taskstatus = 'incomplete';
        }
        console.log(tasktime);
        console.log(taskstatus);
        console.log(taskdesc);
        console.log(taskid);
        console.log(sortid);
        
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $.ajax({
            type: 'POST',
            url: ajaxurl,
            data: {
                taskid: taskid,
                task_estimated_time: tasktime,
                task_brief: taskdesc,
                task_status: taskstatus,
                sortid: sortid,
                projectid: projectid
            },
            success: function (response) {
                //console.log(response);
                var parentli = textarea.parent().parent().parent();
                var returned = $.parseJSON(response.request);
                var items = $('#project-tasks ol li');
                
                var sortid = items.length - 1;
                var attrtaskid = textarea.parent().parent().attr('data-taskid');
                var attrsortid = textarea.parent().parent().attr('data-sortid');
                console.log(attrsortid);
                
                if(attrtaskid === undefined){
                    textarea.parent().parent().attr('data-taskid', response.taskid);
                }
                if(attrsortid === undefined){
                    textarea.parent().parent().attr('data-sortid', sortid );
                }
                
            }
        });

    }

    function removeTask(textarea) {
        var ajaxurl = $("#deletetask_url").val();
        var taskid = textarea.parent().parent().attr('data-taskid');
        if (taskid != null) {

            console.log('remove task ' + taskid);

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            $.ajax({
                type: 'POST',
                url: ajaxurl,
                data: {
                    taskid: taskid,
                },
                success: function (response) {
                    //console.log(response);
                }
            });

        }
    }



})(jQuery);
