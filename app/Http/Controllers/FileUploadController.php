<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use App\Settings;

class FileUploadController extends Controller
{
	function index()
	{
		return view('file_upload');
	}

    public function companyLogoStore(Request $request)
    {
        $image = $request->file('file');
        $imageName = $image->getClientOriginalName();
        $image->move(public_path('images'),$imageName);
        $setting = Settings::where('name', 'company_logo')->first();
        if(isset($setting)){
	        $setting->value = 'images/'.$imageName;
	        $setting->save();
        } else {
	        $s = new Settings();
	        $s->name = 'company_logo';
	        $s->value = 'images/'.$imageName;
	        $s->save();
    	}

        return response()->json(['success' => 'images/'.$imageName]);
    }
    public function companyLogoDestroy(Request $request)
    {
        $filename =  $request->get('filename');
        Settings::where('name', 'company_logo')->delete();
        $path = public_path().'/images/'.$filename;
        if (file_exists($path)) {
            unlink($path);
        }
        return $filename;  
    }

    public function saveImage($file, $save_loc, $filename){
        $return = array(
            'original' => false,
            'thumbnail' => false,
            'error' => false,
        );

        $file->move($save_loc, $filename);
        $public_path = $save_loc.'/'.$filename;

        try {
            // Use the Tinify API client.
            \Tinify\setKey("77ziLYOywAGhdNZw7qOxbUgX1zVV0QOn");
            \Tinify\validate();

            $source = \Tinify\fromFile($public_path);
            $source->toFile($public_path);

            $medium = str_replace('.', '_375x245.', $filename);
            $public_medium_path = $save_loc.'/'.$medium;
            $resized = $source->resize(array(
                "method" => "cover",
                "width" => 375,
                "height" => 245
            ));
            $resized->toFile($public_medium_path);

            $thumbnail = str_replace('.', '_80x80.', $filename);
            $public_thumb_path = $save_loc.'/'.$thumbnail;
            $resizedsquare = $source->resize(array(
                "method" => "thumb",
                "width" => 80,
                "height" => 80
            ));
            $resized->toFile($public_thumb_path);

            $return['original'] = 'Original image "'.$filename.'" optimized & saved.';
            $return['medium'] = 'Medium image "'.$medium.'" optimized & saved.';
            $return['thumbnail'] = 'Thumbnail image "'.$thumbnail.'" optimized & saved.';

        } catch(\Tinify\AccountException $e) {
            // Verify your API key and account limit.
            $return['error'] = $e->getMessage();
        } catch(\Tinify\ClientException $e) {
            // Check your source image and request options.
            $return['error'] = $e->getMessage();
        } catch(\Tinify\ServerException $e) {
            // Temporary issue with the Tinify API.
            $return['error'] = $e->getMessage();
        } catch(\Tinify\ConnectionException $e) {
            // A network connection error occurred.
            $return['error'] = $e->getMessage();
        } catch(Exception $e) {
            // Something else went wrong, unrelated to the Tinify API.
            $return['error'] = $e->getMessage();
        }
        return $return;
    }
    public function getThumbnailUrl($image_url){
        $image0_name = basename($image_url);
        $image0_url = str_replace($image0_name, "", $image_url);
        $thumb_name = str_replace(".", "_80x80.", $image0_name);
        return $image0_url.$thumb_name;
    }
    public function getMediumUrl($image_url){
        $image0_name = basename($image_url);
        $image0_url = str_replace($image0_name, "", $image_url);
        $thumb_name = str_replace(".", "_375x245.", $image0_name);
        return $image0_url.$thumb_name;
    }



}
