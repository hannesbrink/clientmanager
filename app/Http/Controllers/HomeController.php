<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Settings;
use App\Projects;
use App\Tasks;
use App\Invoics;
use App\Quotes;
use App\User;
use Log;
use Auth; 

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware(['auth', 'verified']);
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('dashboard');
    }


    public function projects()
    {
        $client = Auth::user();
        $projects = Projects::where('user_id', $client->id)->orderBy('id', 'DESC')->get();
        return view('projects', compact(
            'client',
            'projects'
        ));
    }
    public function projectsEdit($id)
    {
        $client = Auth::user();
        $project = Projects::find($id);
        return view('projects.edit', compact(
            'client',
            'project'
        ));
    }
    public function projectsAdd(Request $request)
    {

        $name = ($request->has('project_name'))? $request->input('project_name'): '';
        $brief = ($request->has('project_brief'))? $request->input('project_brief'): '';
        $estimated_time = ($request->has('project_estimated_time'))? $request->input('project_estimated_time'): '';
        $lapsed_time = ($request->has('project_lapsed_time'))? $request->input('project_lapsed_time'): '';
        $due_date = ($request->has('project_due_date'))? $request->input('project_due_date'): '';
        $user_id = ($request->has('project_user_id'))? $request->input('project_user_id'): '';
        /*$email = ($request->has('client_email'))? $request->input('client_email'): '';
        $color = ($request->has('client_color'))? $request->input('client_color'): '';*/

        $p = new Projects();
        $p->name = $name;
        $p->brief = $brief;
        $p->estimated_time = $estimated_time;
        $p->lapsed_time = $lapsed_time;
        $p->due_date = $due_date;
        $p->user_id = $user_id;
        //$p->email = $email;
        //$p->color = $color;
        $p->save();

        return redirect('projects')->with('status', 'Project created.');
    }
    public function projectsUpdate(Request $request, $id)
    {

        $name = ($request->has('project_name'))? $request->input('project_name'): '';
        $brief = ($request->has('project_brief'))? $request->input('project_brief'): '';
        //$estimated_time = ($request->has('project_estimated_time'))? $request->input('project_estimated_time'): '';
        //$lapsed_time = ($request->has('project_lapsed_time'))? $request->input('project_lapsed_time'): '';
        $due_date = ($request->has('project_due_date'))? $request->input('project_due_date'): '';
        //$user_id = ($request->has('project_user_id'))? $request->input('project_user_id'): '';

        $p = Projects::find( $id );
        $p->name = $name;
        $p->brief = $brief;
        //$p->estimated_time = $estimated_time;
        //$p->lapsed_time = $lapsed_time;
        $p->due_date = $due_date;
        //$p->user_id = $user_id;
        $p->save();

        return back()->with('status', 'Project Updated.');
    }


    public function profile()
    {
        $client = Auth::user();
        return view('profile', compact('client'));
    }
    public function profileUpdate(Request $request, $user_id)
    {

        $name = ($request->has('client_name'))? $request->input('client_name'): '';
        $email = ($request->has('client_email'))? $request->input('client_email'): '';
        $color = ($request->has('client_color'))? $request->input('client_color'): '';
 
        $client = User::find( $user_id );
        $client->name = $name;
        $client->email = $email;
        $client->color = $color;
        if($request->has('client_extra')):
            $client->extra = $request->input('client_extra');
        endif;
        $client->save();

        return redirect('profile')->with('status', 'Profile Updated.');
    }


}
