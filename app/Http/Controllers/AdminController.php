<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Settings;
use App\Projects;
use App\Tasks;
use App\Invoices;
use App\Quotes;
use App\User;
use Log;
use Auth;
use PDF;

class AdminController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function admin()
    {
        return view('admin');
    }


    public function settings()
    {
    	$settings = Settings::all()->toArray();
        return view('admin.settings', compact(
        	'settings'
        ));
    }
    public function settingsUpdate(Request $request)
    {

    	//echo '<pre>'.print_r($request->all(), true).'</pre>';
        $settings = $request->all();
        foreach($settings as $key => $value){
        	if($key != '_token'){
	        	$setting = Settings::where('name', $key)->first();
	        	if(isset($setting)){
		        	//$setting->name = $key;
		        	$setting->value = $value;
		        	$setting->save();        		
	        	} else {
		        	$s = new Settings;
		        	$s->name = $key;
		        	$s->value = $value;
		        	$s->save();        		
	        	}
 	        }
        }
        return back()->with('status', 'Settings updated.');
    }


    public function clients()
    {
        $clients = User::where('type', 'default')->orderby('created_at', 'desc')->get();
        return view('admin.clients', compact('clients'));
    }

    public function clientEdit($id)
    {
        $client = User::find($id);
        return view('admin.clients.edit', compact('client'));
    }
    public function clientUpdate(Request $request, $id)
    {
        
        $name = ($request->has('client_name'))? $request->input('client_name'): '';
        $company = ($request->has('client_company'))? $request->input('client_company'): '';
        $email = ($request->has('client_email'))? $request->input('client_email'): '';
        $color = ($request->has('client_color'))? $request->input('client_color'): '';
        $status = ($request->has('client_status'))? $request->input('client_status'): '';

        $client = User::find($id);
        $client->name = $name;
        $client->company = $company;
        $client->email = $email;
        $client->color = $color;
        $client->status = $status;
        if($request->has('client_extra')):
            $client->extra = $request->input('client_extra');
        endif;
        $client->save();

        return redirect('admin/clients')->with('status','Client Updated.');
    }

    public function clientDelete($id)
    {
        $client = User::find($id);
        $client->delete();
        return redirect('admin/clients')->with('status','Client Deleted.');
    }


    public function projects()
    {
        $clients = User::where([
            [ 'type', 'default' ], 
            [ 'status', 'active' ], 
        ])->orderBy('name', 'asc')->get();
        
        $client = Auth::user();
        
        $projects = Projects::orderby('due_date', 'desc')->get();
        foreach($projects as $project){
            $tasks = Tasks::where('project_id', $project->id)->get();
            $taskcount = ( isset($tasks) )? count($tasks): 0;
            $hours = 0;
            if($taskcount){
                foreach($tasks as $task){
                    if($task->estimate_time != 'Heading'):
                    $hours += $task->estimate_time;
                    endif;
                }
            }
            $project_total = ($hours * $project->rate); 
            $project->total = $project_total;
            $project->save();
        }
        return view('admin.projects', compact(
            'client',
            'clients',
            'projects'
        ));
    }
    public function filterProjectsByTotal(){
        $clients = User::where([
            [ 'type', 'default' ], 
            [ 'status', 'active' ], 
        ])->orderBy('name', 'asc')->get();
        
        $client = Auth::user();
        
        $projects = Projects::orderby('total', 'desc')->get();
        foreach($projects as $project){
            $tasks = Tasks::where('project_id', $project->id)->get();
            $taskcount = ( isset($tasks) )? count($tasks): 0;
            $hours = 0;
            if($taskcount){
                foreach($tasks as $task){
                    $hours += $task->estimate_time;
                }
            }
            $project_total = ($hours * $project->rate); 
            $project->total = $project_total;
            $project->save();
        }
        
        return view('admin.projects', compact(
            'client',
            'clients',
            'projects'
        ));
    }
    
    public function clientProjects($id)
    {
        $clients = User::where([
            [ 'type', 'default' ], 
            [ 'status', 'active' ], 
        ])->get();
        $client = Auth::user();
        $projects = Projects::where('user_id', $id)->orderBy('id', 'DESC')->get();
        return view('admin.projects', compact(
            'client',
            'clients',
            'projects'
        ));
    }

    public function projectsEdit($id)
    {
        $project = Projects::find($id);
        $client = User::find($project->user_id);
        $tasks = Tasks::where('project_id', $id)->orderBy('sort_index', 'ASC')->get();
        $quotes = Quotes::where('project_id', $id)->orderBy('created_at', 'DESC')->get();
        $invoices = Invoices::where('project_id', $id)->orderBy('created_at', 'DESC')->get();

        if( $project->rate == 0 ){
            $project->rate = Settings::get_setting('company_rate');
            $project->save();
        }
        return view('admin.projects.edit', compact(
            'client',
            'project',
            'tasks',
            'quotes',
            'invoices'
        ));
    }
    public function projectsAdd(Request $request)
    {

        $name = ($request->has('project_name'))? $request->input('project_name'): '';
        $brief = ($request->has('project_brief'))? $request->input('project_brief'): '';
        $estimated_time = ($request->has('project_estimated_time'))? $request->input('project_estimated_time'): '';
        $lapsed_time = ($request->has('project_lapsed_time'))? $request->input('project_lapsed_time'): '';
        $due_date = ($request->has('project_due_date'))? $request->input('project_due_date'): '';
        $user_id = ($request->has('project_user_id'))? $request->input('project_user_id'): '';
        $project_rate = ($request->has('project_rate'))? $request->input('project_rate'): '';

        /*$email = ($request->has('client_email'))? $request->input('client_email'): '';
        $color = ($request->has('client_color'))? $request->input('client_color'): '';*/

        $p = new Projects();
        $p->name = $name;
        $p->brief = $brief;
        $p->estimated_time = $estimated_time;
        $p->lapsed_time = $lapsed_time;
        $p->due_date = $due_date.' 00:00:00';
        $p->rate = $project_rate;
        $p->user_id = $user_id;
        //$p->email = $email;
        //$p->color = $color;
        $p->save();

        return redirect('admin/projects')->with('status', 'Project created.');
    }
    public function projectsUpdate(Request $request, $id)
    {

        $name = ($request->has('project_name'))? $request->input('project_name'): '';
        $brief = ($request->has('project_brief'))? $request->input('project_brief'): '';
        //$estimated_time = ($request->has('project_estimated_time'))? $request->input('project_estimated_time'): '';
        //$lapsed_time = ($request->has('project_lapsed_time'))? $request->input('project_lapsed_time'): '';
        $due_date = ($request->has('project_due_date'))? $request->input('project_due_date'): '';
        //$user_id = ($request->has('project_user_id'))? $request->input('project_user_id'): '';
        $project_rate = ($request->has('project_rate'))? $request->input('project_rate'): '';
        $project_paid_amount = ($request->has('project_paid_amount'))? $request->input('project_paid_amount'): 0;
        $project_status = ($request->has('project_status'))? $request->input('project_status'): 'quote';

        $p = Projects::find( $id );
        $p->name = $name;
        $p->brief = $brief;
        //$p->estimated_time = $estimated_time;
        //$p->lapsed_time = $lapsed_time;
        $p->due_date = str_replace(' 00:00:00', '', $due_date).' 00:00:00';
        $p->status = $project_status;
        $p->rate = $project_rate;
        $p->paid_amount = $project_paid_amount;
        //$p->user_id = $user_id;
        $p->save();

        return back()->with('status', 'Project Updated.');
    }
    public function projectsDelete(Request $request, $id){
        
        /*delete project tasks */
        Tasks::where('project_id', $id)->delete();

        /*delete project quotes */
        Quotes::where('project_id', $id)->delete();
        
        /*delete project invoices */
        Invoices::where('project_id', $id)->delete();
       
        /*delete project*/
        $p = Projects::find($id);
        $p->delete();
        
        return redirect('admin/projects')->with('status', 'Project <strong>"'.$p->name.'"</strong> and all it\'s related data has been deleted.');
    }
    
    
    public function projectsTaskAdd(Request $request, $id)
    {
        $p = Projects::find($id);
        $name = (isset($p))? $p->name.'_task': '_task';
        $task_brief = ($request->has('task_brief'))? $request->input('task_brief'): '';
        $task_estimated_time = ($request->has('task_estimated_time'))? $request->input('task_estimated_time'): '';

        /*$email = ($request->has('client_email'))? $request->input('client_email'): '';
        $color = ($request->has('client_color'))? $request->input('client_color'): '';*/

        $t = new Tasks();
        $t->name = $name;
        $t->brief = $task_brief ;
        $t->estimate_time = $task_estimated_time;
        $t->lapsed_time = '0.00hrs';
        $t->status = 'incomplete';
        $t->due_date = $p->due_date;
        $t->project_id = $id;
        $t->save();

        return redirect('admin/projects/edit/'.$id)->with('status', 'Project Task Created.');
    }
    public function projectsTaskSortJson(Request $request){
        
        $input = $request->all();
        $orderjson = (isset($input['order']))? $input['order']: '';
        $taskids = json_decode($orderjson);
        
        $cnt=0; foreach($taskids as $taskid){
            
            $t = Tasks::find($taskid);
            $t->sort_index = $cnt;
            $t->save();
            
        $cnt++; }
        
        return response()->json([
            'success' => 'Project tasks ordering updated.',
            'request' => json_encode($input),
            //'taskid' => $t->id
        ]);
         
    }
    public function projectsTaskAddJson(Request $request)
    {
        $input = $request->all();
        Log::debug( print_r($input,true) );
        $id = (isset($input['projectid']))? $input['projectid']: '';
        $p = Projects::find($id);
        $name = (isset($p))? $p->name.'_task': '_task';
        $task_brief = (isset($input['task_brief']))? $input['task_brief']: '';
        $task_estimated_time = (isset($input['task_estimated_time']))? $input['task_estimated_time']: '';
        $sortid = (isset($input['sortid']))? $input['sortid']: 0;
        $task_status = (isset($input['task_status']))? $input['task_status']: 'incomplete';

        
        $taskid = (isset($input['taskid']))? $input['taskid']: '';
        
        if($id != ''){
            if($taskid == 0){
                $t = new Tasks();
            } else {
                $t = Tasks::find($taskid);
            }        
            $t->name = $name;
            $t->brief = $task_brief ;
            $t->estimate_time = $task_estimated_time;
            $t->lapsed_time = '0.00';
            $t->status = $task_status;
            $t->due_date = $p->due_date;
            $t->sort_index = $sortid;
            $t->project_id = $id;
            $t->save();
        }

        return response()->json([
            'success' => 'Project Task Updated.',
            'request' => json_encode($input),
            'taskid' => $t->id
        ]);
        //return redirect('admin/projects/edit/'.$id)->with('status', 'Project Task Created.');
    }
    public function taskDelete(Request $request)
    {
        $id = ($request->has('taskid'))? $request->input('taskid'): '';
        $t = Tasks::find($id);
        $t->delete();
        
        return response()->json([
            'success' => 'Task Deleted.'
        ]);
    }

    public function profile()
    {
        $client = Auth::user();
        return view('admin.profile', compact('client'));
    }
    public function profileUpdate(Request $request)
    {
        return redirect('profile')->with('status','Profile Updated.');
    }

    public function projectsQuoteDelete(Request $request, $id){
        $quote = Quotes::find($id);
        $quote->delete();
        return back()->with('status', 'Quote deleted.');
    }
    public function projectsQuotePDF($id, $view_type){
        
        $quote_start = Settings::get_setting('company_quote_start');
        
        $timestamp = time();
        $project = Projects::find($id);
        $client = User::find($project->user_id);
        $filename = strtolower($client->name).'_'.strtolower($project->name).'_'.date('dmY', $timestamp).'_quote';
        $tasks = Tasks::where('project_id', $project->id)->orderBy('sort_index', 'ASC')->get();
        
        $quote = new Quotes();
        $quote->name = $filename;
        $quote->user_id = $client->id;
        $quote->project_id = $id;
        $quote->save(); 
        
        $quote_index = ($quote_start + $quote->id);
        
        $data = [
            'quote_id' => $quote_index,
            'timestamp' => $timestamp,
            'title' => $client->name.' - '.$project->name.' Quote',
            'project' => $project,
            'client' => $client,
            'tasks' => $tasks,
            'content' => "Thank you for your business!"
        ];

        if($view_type === 'download') {
            $pdf = PDF::loadView('admin.projects.quote', ['data' => $data]);
            $filename = $filename.'.pdf';
            return $pdf->download($filename);
        } else {
            $view = View('admin.projects.quote', ['data' => $data]);
            $pdf = \App::make('dompdf.wrapper');
            $pdf->loadHTML($view->render());
            return $pdf->stream();
        }



    }
    public function projectsQuoteOpen($id, $view_type){
    
        $quote_start = Settings::get_setting('company_quote_start');

        $quote = Quotes::find($id);
        $quote_index = ($quote_start + $quote->id);
        $timestamp = $quote->created_at->timestamp;
        $project = Projects::find($quote->project_id);
        $client = User::find($quote->user_id);
        $filename = strtolower($client->name).'_'.strtolower($project->name).'_'.date('dmY', $timestamp).'_quote';
        $tasks = Tasks::where('project_id', $project->id)->orderBy('sort_index', 'ASC')->get();
        
        $data = [
            'quote_id' => $quote_index,
            'timestamp' => $timestamp,
            'title' => $client->name.' - '.$project->name.' Quote',
            'project' => $project,
            'client' => $client,
            'tasks' => $tasks,
            'content' => "Thank you for your business!"
        ];
        
        if($view_type === 'download') {
            $pdf = PDF::loadView('admin.projects.quote', ['data' => $data]);
            $filename = $filename.'.pdf';
            return $pdf->download($filename);
        } else {
            $view = View('admin.projects.quote', ['data' => $data]);
            $pdf = \App::make('dompdf.wrapper');
            $pdf->loadHTML($view->render());
            return $pdf->stream();
        }
        
    }

    public function projectsInvoiceDelete(Request $request, $id){
        $i = Invoices::find($id);
        $i->delete();
        return back()->with('status', 'Invoice deleted.');
    }
    public function projectsInvoicePDF($id, $view_type){
        
        $invoice_start = Settings::get_setting('company_invoice_start');
        
        $timestamp = time();
        $project = Projects::find($id);
        $client = User::find($project->user_id);
        $tasks = Tasks::where('project_id', $project->id)->orderBy('sort_index', 'ASC')->get();
        $filename = strtolower($client->name).'_'.strtolower($project->name).'_'.date('dmY', $timestamp).'_invoice';
        $subtotal = 0;
        $totalhrs = 0;
        foreach($tasks as $task):
            if($task->estimate_time != 'Heading'){
            $totalhrs += $task->estimate_time;
            $tasktotal = ($task->estimate_time * $project->rate);
            $subtotal = $subtotal + $tasktotal; 
            }            
        endforeach;
        $balance = $subtotal - number_format($project->paid_amount, 2, '.', '');
        
        $invoice = new Invoices();
        $invoice->name = $filename;
        $invoice->user_id = $client->id;
        $invoice->project_id = $id;
        $invoice->save(); 
        
        $invoice_index = ($invoice_start+$invoice->id);
        
        $data = [
            'invoice_id' => $invoice_index,
            'balance' => $balance,
            'subtotal' => $subtotal,
            'timestamp' => $timestamp,
            'title' => $filename,
            'project' => $project,
            'client' => $client,
            'tasks' => $tasks,
            'content' => "Thank you for your business!"
        ];

        if($view_type === 'download') {
            $pdf = PDF::loadView('admin.projects.invoice', ['data' => $data]);
            $filename = $filename.'pdf';
            return $pdf->download($filename);
        } else {
            $view = View('admin.projects.invoice', ['data' => $data]);
            $pdf = \App::make('dompdf.wrapper');
            $pdf->loadHTML($view->render());
            return $pdf->stream();
        }

    }
    public function projectsInvoiceOpen($id, $view_type){
        
        $invoice_start = Settings::get_setting('company_invoice_start');
        
        $invoice = Invoices::find($id);
        $invoice_index = ($invoice_start+$invoice->id);
        
        $timestamp = $invoice->created_at->timestamp;
        $project = Projects::find($invoice->project_id);
        $client = User::find($invoice->user_id);
        $tasks = Tasks::where('project_id', $project->id)->orderby('sort_index')->get();

        $filename = strtolower($client->name).'_'.strtolower($project->name).'_'.date('dmY', $timestamp).'_invoice';
        $subtotal = 0;
        $totalhrs = 0;
        foreach($tasks as $task):
            
            if(strtolower($task->estimate_time) !== 'heading'){
                $totalhrs += $task->estimate_time;
                $tasktotal = ($task->estimate_time * $project->rate);
                $subtotal = $subtotal + $tasktotal; 
            }
            
        endforeach;
        $balance = $subtotal - number_format($project->paid_amount, 2, '.', '');
       
        $data = [
            'invoice_id' => $invoice_index,
            'balance' => $balance,
            'subtotal' => $subtotal,
            'timestamp' => $timestamp,
            'title' => $filename,
            'project' => $project,
            'client' => $client,
            'tasks' => $tasks,
            'content' => "Thank you for your business!"
        ];

        if($view_type === 'download') {
            $pdf = PDF::loadView('admin.projects.invoice', ['data' => $data]);
            $filename = $filename.'pdf';
            return $pdf->download($filename);
        } else {
            $view = View('admin.projects.invoice', ['data' => $data]);
            $pdf = \App::make('dompdf.wrapper');
            $pdf->loadHTML($view->render());
            return $pdf->stream();
        }

    }


}