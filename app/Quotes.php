<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Quotes extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'quotes';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'user_id',
        'project_id',
    ];
}
