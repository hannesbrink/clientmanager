<?php return array (
  'sans-serif' => array(
    'normal' => $rootDir . '/lib/fonts/Helvetica',
    'bold' => $rootDir . '/lib/fonts/Helvetica-Bold',
    'italic' => $rootDir . '/lib/fonts/Helvetica-Oblique',
    'bold_italic' => $rootDir . '/lib/fonts/Helvetica-BoldOblique',
  ),
  'times' => array(
    'normal' => $rootDir . '/lib/fonts/Times-Roman',
    'bold' => $rootDir . '/lib/fonts/Times-Bold',
    'italic' => $rootDir . '/lib/fonts/Times-Italic',
    'bold_italic' => $rootDir . '/lib/fonts/Times-BoldItalic',
  ),
  'times-roman' => array(
    'normal' => $rootDir . '/lib/fonts/Times-Roman',
    'bold' => $rootDir . '/lib/fonts/Times-Bold',
    'italic' => $rootDir . '/lib/fonts/Times-Italic',
    'bold_italic' => $rootDir . '/lib/fonts/Times-BoldItalic',
  ),
  'courier' => array(
    'normal' => $rootDir . '/lib/fonts/Courier',
    'bold' => $rootDir . '/lib/fonts/Courier-Bold',
    'italic' => $rootDir . '/lib/fonts/Courier-Oblique',
    'bold_italic' => $rootDir . '/lib/fonts/Courier-BoldOblique',
  ),
  'helvetica' => array(
    'normal' => $rootDir . '/lib/fonts/Helvetica',
    'bold' => $rootDir . '/lib/fonts/Helvetica-Bold',
    'italic' => $rootDir . '/lib/fonts/Helvetica-Oblique',
    'bold_italic' => $rootDir . '/lib/fonts/Helvetica-BoldOblique',
  ),
  'zapfdingbats' => array(
    'normal' => $rootDir . '/lib/fonts/ZapfDingbats',
    'bold' => $rootDir . '/lib/fonts/ZapfDingbats',
    'italic' => $rootDir . '/lib/fonts/ZapfDingbats',
    'bold_italic' => $rootDir . '/lib/fonts/ZapfDingbats',
  ),
  'symbol' => array(
    'normal' => $rootDir . '/lib/fonts/Symbol',
    'bold' => $rootDir . '/lib/fonts/Symbol',
    'italic' => $rootDir . '/lib/fonts/Symbol',
    'bold_italic' => $rootDir . '/lib/fonts/Symbol',
  ),
  'serif' => array(
    'normal' => $rootDir . '/lib/fonts/Times-Roman',
    'bold' => $rootDir . '/lib/fonts/Times-Bold',
    'italic' => $rootDir . '/lib/fonts/Times-Italic',
    'bold_italic' => $rootDir . '/lib/fonts/Times-BoldItalic',
  ),
  'monospace' => array(
    'normal' => $rootDir . '/lib/fonts/Courier',
    'bold' => $rootDir . '/lib/fonts/Courier-Bold',
    'italic' => $rootDir . '/lib/fonts/Courier-Oblique',
    'bold_italic' => $rootDir . '/lib/fonts/Courier-BoldOblique',
  ),
  'fixed' => array(
    'normal' => $rootDir . '/lib/fonts/Courier',
    'bold' => $rootDir . '/lib/fonts/Courier-Bold',
    'italic' => $rootDir . '/lib/fonts/Courier-Oblique',
    'bold_italic' => $rootDir . '/lib/fonts/Courier-BoldOblique',
  ),
  'dejavu sans' => array(
    'bold' => $rootDir . '/lib/fonts/DejaVuSans-Bold',
    'bold_italic' => $rootDir . '/lib/fonts/DejaVuSans-BoldOblique',
    'italic' => $rootDir . '/lib/fonts/DejaVuSans-Oblique',
    'normal' => $rootDir . '/lib/fonts/DejaVuSans',
  ),
  'dejavu sans mono' => array(
    'bold' => $rootDir . '/lib/fonts/DejaVuSansMono-Bold',
    'bold_italic' => $rootDir . '/lib/fonts/DejaVuSansMono-BoldOblique',
    'italic' => $rootDir . '/lib/fonts/DejaVuSansMono-Oblique',
    'normal' => $rootDir . '/lib/fonts/DejaVuSansMono',
  ),
  'dejavu serif' => array(
    'bold' => $rootDir . '/lib/fonts/DejaVuSerif-Bold',
    'bold_italic' => $rootDir . '/lib/fonts/DejaVuSerif-BoldItalic',
    'italic' => $rootDir . '/lib/fonts/DejaVuSerif-Italic',
    'normal' => $rootDir . '/lib/fonts/DejaVuSerif',
  ),
  'opensans' => array(
    'normal' => $fontDir . '/6d2dc3d33ac60391b9c05306015bf3df',
  ),
  'opensanssemibold' => array(
    'normal' => $fontDir . '/0945e88fa8d2dd3063853768b8c2ae25',
  ),
  'lato' => array(
    'normal' => $fontDir . '/715465684905d3b8ce2f835853fdef11',
  ),
  'latobold' => array(
    'normal' => $fontDir . '/421d24aaffcebfcde00a0347fbdc718e',
  ),
  'maax' => array(
    'normal' => $fontDir . '/2cad8b95eb037d19991789924a4a23fb',
  ),
  'maaxmedium' => array(
    'normal' => $fontDir . '/43e6f7dd3af43eb243bc57ee6b3cc07c',
  ),
  'maaxbold' => array(
    'normal' => $fontDir . '/9444c04635b23f7402e5c37850dfde0e',
  ),
  'work sans' => array(
    'normal' => $fontDir . '/a9dcc259db9a6a52e22d9bbe4f56fd9c',
  ),
  'work sans medium' => array(
    'normal' => $fontDir . '/360e16248698e54165725c5f372f2af6',
  ),
  'work sans semibold' => array(
    'normal' => $fontDir . '/cb92c486035a56e4903f35dcbe32e8f8',
  ),
  'work sans bold' => array(
    'normal' => $fontDir . '/fd6f4ddd3d787eba2a8f35011a76d2c3',
  ),
  'amiko' => array(
    'normal' => $fontDir . '/a5f2ac6e55c10cf60ce25bb56352beb1',
  ),
  'amiko bold' => array(
    'normal' => $fontDir . '/b76c073d46d1d49a2a8b2ae37e1d6826',
  ),
  'amiko semibold' => array(
    'normal' => $fontDir . '/357f2c07ec3ac40baead2e6b5f0b6a46',
  ),
) ?>